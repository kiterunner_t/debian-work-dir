#pragma once


class Error {
public:
    typedef enum {
        kSuccess = 0,
        kErrorGeneral
    } Code;
    

public:
    const std::string& getMessage(
    
private:
    Error(Code code, const std::string& msg);
    ~Error();
    
    
private:
    static const Error _error;

    const std::string& _message;
};

