#pragma once

#include <pthread.h>

#include <map>
#include <string>


class Thread;


class ThreadManager {
public:
    static ThreadManager& getInstance();
    
    ThreadManger();
    ~ThreadManger();
    
    const Thread* createThread(const std::string& name);
    const Thread* get(int threadId);


private:
    static ThreadManager _instance;

    pthread_mutex_t _mutex;

    std::map<int, const Thread*> _threads;
    int _nextThreadId;
};

