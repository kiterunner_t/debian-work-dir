// http://blog.csdn.net/wzwind/article/details/7231230
// http://blog.csdn.net/myxmu/article/details/9467737
// http://www.educity.cn/os/1605428.html

// http://blog.csdn.net/xiaoyanghuaban/article/details/46494197
// http://blog.csdn.net/xiaoyanghuaban/article/details/46493861
// http://core-analyzer.sourceforge.net/index_files/Page1510.html

#include <stdlib.h>

#include <pthread.h>
#include <signal.h>

#include "Log.h"
#include "SignalManager.h"


void SignalManager::start()
{
    _signalManager._create();
}


void SignalManager::_create()
{
    sigemptyset(&_blocked);

    for (int i = 0; i < _blockedSigsetsLenth; ++i) {
        SignalNo& signalNo = _blockedSigsets[i];
        sigaddset(&_blocked, signalNo.getSignalNo());
    }
    
    ret = pthread_sigmask(SIG_BLOCK, &_blocked, NULL);
    if (ret != 0) {
        log_error("signal set mask error, error_code=%d", ret);
        exit(-1);
    }
    
    ret = pthread_create(&_thread, NULL, &_signalHandler, (void*) &_blocked);
    if (ret != 0) {
        log_error("create thread for sinal handle error, error_code=%d", ret);
        exit(-1);
    }
}


void* SignalManager::_signalHandler(void* arg)
{
    sigset_t* blocked = (sigset_t*) arg;
    int ret;
    int signo;
    
    for ( ; ; ) {
        ret = sigwait(blocked, &signo);
        if (ret != 0) {
            log_error("sig_thread in sigwait failed, error_code=%d", ret);
            exit(-1);
        }
        
        if (_isBlockedSignal(signo) == true) {
            log_error("SignalManager recieved singal/%d, exited", signo);
            abort();

        } else {
            log_warn("SignalManager recieved non_blocked siganl/%d", signo);
        }
    }
}


bool SignalManager::_isBlockedSignal(int signo)
{
    bool ret = false;
    for (int i = 0; i < _blockedSigsetsLenth; ++i) {
        SignalNo& signalNo = _blockedSigsets[i];
        if (signo == signalNo.getSignalNo()) {
            ret = true;
            break;
        }
    }
    
    return ret;
}

