// g++ -lpthread TestSignalManager.cpp SignalManager.cpp -o mcore

#include <stdio.h>

#include <pthread.h>
#include <unistd.h>

#include "SignalManager.h"


void* core_dump_thread(void* arg)
{
    int n = (long) arg;
    long long ret = 0;
    int max = 1 * 1000 * 1000;
    for (int i = 0; i < max; ++i) {
        ret += i;
        if (ret == max - 1) {
            ret = ret / n; // core dump
        }
    }
}


void* print_thread(void* arg)
{
    printf("+");
}


int main(int argc, const char** argv)
{
    SignalManager::start();
    
    long a = 0;
    pthread_t pt1;
    pthread_t pt2;
    pthread_t t;
    pthread(&pt1, NULL, &print_thread, NULL);
    pthread(&pt2, NULL, &print_thread, NULL);
    pthread(&t, NULL, &core_dump_thread, (void*) a);

    for ( ; ; ) {
        printf(".");
        sleep(10);
    }
    
    return 0;
}

