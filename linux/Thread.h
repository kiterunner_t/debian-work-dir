
class Thread {
public:
    static const int kInvalidId = 0xFFFFFFFF;

    typedef enum {
        kInit = 0,
        kRunning,
        kEnd,
        kMax
    } Status;
    
    typedef (void*)(*HandlerPt)(void* arg);


public:
    Thread(int id, const std::string& name, HandlerPt handler);
    ~Thread();
    
    Error::Code start();
    void join();
    
    int getId() const;
    const std::string& getName() const;


private:
    pthread_mutex_t _mutex;
    pthread_t _handle;
    int _id;
    const std::string _name;
    HandlerPt _handler;
    void* _arg;
};