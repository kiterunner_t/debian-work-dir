#pragma once

#include <pthread.h>
#include <signal.h>


class SignalNo;


// 仅仅用于多线程的信号管理
class SignalManager {
public:
    static void start();


private:
    SignalManager() {}
    ~SignalManager() {}
    
    void _create();
    void* _signalHandler(void* arg);
    bool _isBlockedSignal(int signo);

    
private:
    #define SIGNAL_NO(signo) SignalNo(signo, ##signo)

    static const SignalNo _blockedSigsets[] = {
        SIGNAL_NO(SIGABRT),
        SIGNAL_NO(SIGSEGV),
        SIGNAL_NO(SIGEMT),
        SIGNAL_NO(SIGFPE),
        SIGNAL_NO(SIGILL),
        SIGNAL_NO(SIGIOT),
        SIGNAL_NO(SIGSYS),
        SIGNAL_NO(SIGTRAP),

        SIGNAL_NO(SIGTERM),
        SIGNAL_NO(SIGQUIT),
    };
    
    static int _blockedSigsetsLenth = sizeof(_blockedSigsets) / sizeof(SignalNo);
    static SignalManager _signalManager;
    
    pthread_t _thread;
    sigset_t _blocked;
};


class SignalNo {
public:
    SignalNo(int signo, const char* name) {
        this._signo = signo;
        this._name = name;
    }
    
    ~SignalNo() {}
    
    int getSigNo() const { return _signo; }
    const char* getName() const { return _name; }


private:
    int _signo;
    const char* _name;
};

