
ThreadManager ThreadManager::_instance;


ThreadManager::ThreadManager()
    : _mutex(),
      _threads(),
      _nextThreadId(1)
{
}


ThreadManager::~ThreadManager()
{
    // 退出
}


const Thread* ThreadManager::createThread(const std::string& name, HandlerPt handler)
{
    Thread* thread = new Thread(Thread::kInvalidId, name, handler);
    
    if (pthread_mutex_lock(&_mutex) != 0) {
        return NULL;
    }
    
    int id = _nextThreadId;
    _nextThreadId++;
    thread.setId(id);
    _threads.put(id, thread);
    
    pthread_mutex_unlock(&_mutex);    
    return thread;
}


const Thread* ThreadManager::get(int threadId)
{
    if (pthread_mutex_lock(&_mutex) != 0) {
        return NULL;
    }
    
    const Thread* thread = _threads.get(threadId);
    pthread_mutex_unlock(&_mutex);
    return thread;
}

