
Thread::Thread(int id, const std::string& name, HandlePt handler, void* arg)
    : _mutex(PTHERAD_MUTEX_INITIAZER),
      _handle(NULL), // only for Linux
      _id(id),
      _name(name),
      _handler(handler),
      _arg(arg)
{
}


Thread::~Thread()
{
}


Error::Code Thread::start()
{
    int ret;
    ret = pthread_create(&_handle, NULL, &_handler, _arg)
    if (ret != 0) {
        return kErrorGeneral;
    }
    
    return kSuccess;
}