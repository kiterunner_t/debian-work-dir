#! /usr/bin/env python
# coding: utf-8

import flask


app = flask.Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.debug = True
    app.run("192.168.239.137")

