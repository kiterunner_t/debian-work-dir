import java.io.IOException;
import java.util.concurrent.*;


public class TestExecutor {
    public static void main(String[] args) throws IOException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 4; i++) {
            service.execute(() -> {
                    for (int j = 0; j < 10; j++) {
                        System.out.println("hi, " + j + ", " + Thread.currentThread().getName());
                    }
                });
        }

        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        System.out.println("all thread complete");
    }
}

