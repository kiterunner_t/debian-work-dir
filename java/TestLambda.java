import java.util.List;
import java.util.Arrays;
import java.util.Collections;


class TestLambda {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
                public void run() {
                    System.out.println("hi");
                }
            };

        new Thread(runnable).start();

        testLambda();

        testMethodRef();
    }


    public static void testLambda() {
        new Thread(() -> System.out.println("hi lambda")).start();
    }


    public static void testMethodRef() {
        List<String> s = Arrays.asList("C", "a", "A", "b");
        Collections.sort(s, String::compareToIgnoreCase);
    }
}

