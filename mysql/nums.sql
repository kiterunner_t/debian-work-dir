delimiter //

create procedure employees.create_nums(cnt int unsigned)
    begin
        declare s int unsigned default 1;
        
        truncate table nums;
        
        while s <= cnt do 
            begin
                insert into nums select s;
                set s = s + 1;
            end;
        end while;
    end
    //

    
delimiter ;
