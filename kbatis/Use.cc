// http://blog.jobbole.com/108135/


/*

*/


class Person: public Resource {
public:


private:
    int _id;
    std::string _name;
};


#define IN
#define OUT


class PersonMapper: public Mapper {
public:
    ///@SQL insert person (id, name) values (#{_id}, #{_name});
    // insert person (id, name) values (?, ?);
    void store(IN Person& person);
    // void store(int id, std::string& name);

    ///@SQL select id, name from person where id=#{id};
    void getOne(IN int id, OUT Person& person);

    ///@SQL select id, name from person;
    void getAll(OUT VectorResource<Person*>& persons);
    
    ///@SQL select id, name from person;
    void getAll(OUT VectorResource<Person>& persons);
};


void PersonMapper::store(IN Person& person)
{
    const char* _personmapper_store_sql = "insert person (id, name) values (?, ?)";
    // 参数绑定
    bind(stmt, 0, person.getId());
    bind(stmt, 1, person.getName().c_str());
    // 执行SQL
}


void PersonMapper::getOne(IN int id, OUT person& person)
{
    const char* _personmapper_getone_sql = "select id, name from person where id=?";
    bind(stmt, 0, id);
    
    // 执行SQL
    
    // 获取返回结果
    sqlite3_stmt* stmt;
    assert(sqlite3_column_type(stmt, 0) == SQLITE_INTEGER);

    person.setId(sqlite3_column_int(stmt, 0));
    person.setName(sqlite3_column_text(stmt, 1));
}


void example()
{
    PersonMapper personMapper;
    
    Person one;
    personMapper.getOne(one);
    cout << one.getId() << ", " << one.getName() << endl;
    
    one.setName("hello");
    personMapper.store(one);
    
    VectorResource<Person*> persons;
    personMapper.getAll(persons);
    Person* p1 = persons.get(0);
    cout << p1->getId() << ", "" << p1->getName() << endl;
    
    VectorResource<Person> personObjs;
    personMapper.getAll(personObjs);
    Person& pObj = personObjs.get(0);
    cout << pObj.getId() << ", " << pObj.getName() << endl;
}

