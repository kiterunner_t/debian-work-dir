// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once


namespace krt {
    
    
typedef enum {
    kSuccess = 0,
    kError,
    kErrorNoData,
    kErrorNotMatch,
    kErrorExceed,
    kErrorFetch,
    
    kErrorMax  // please do not use this error code, cause this is just a flag
} ErrorCode;


class Error {
};


}

