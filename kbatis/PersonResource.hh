// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once

#include <string>

#include "Resource.hh"


namespace krt {


class PersonResource: public Resource {
public:
    PersonResource();  // please do not implement it by yourself
    ~PersonResource() {}


public:
    long _id;
    std::string _name;
};


}

