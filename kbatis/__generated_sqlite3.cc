// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include <cassert>
#include <string>

#include <sqlite3.h>

#include "Error.hh"
#include "PersonResource.hh"
#include "Resource.hh"


namespace krt {


static void __PersonResource__parseResult(void* handle, Resource* r)
{
    sqlite3_stmt* result = static_cast<sqlite3_stmt*>(handle);
    PersonResource* pr = static_cast<PersonResource*>(r);
    int type = sqlite3_column_type(result, 0);
    assert(sqlite3_column_type(result, 0) == SQLITE_INTEGER);
    pr->_id = sqlite3_column_int(result, 0);

    type = sqlite3_column_type(result, 1);
    assert(type == SQLITE_TEXT);
    const char* p = reinterpret_cast<const char*>(sqlite3_column_text(result, 1));
    std::string a(p);
    pr->_name = a;
}


static void __PersonResource__parseResult(void* handle, Resource** r)
{
    sqlite3_stmt* result = static_cast<sqlite3_stmt*>(handle);
    PersonResource* person = new PersonResource();
    int type = sqlite3_column_type(result, 0);
    assert(type == SQLITE_INTEGER);
    person->_id = sqlite3_column_int(result, 0);

    type = sqlite3_column_type(result, 1);
    assert(type == SQLITE_TEXT);
    const char* p = reinterpret_cast<const char*>(sqlite3_column_text(result, 1));
    std::string a(p);
    person->_name = a;

    *r = static_cast<Resource*>(person);
}


PersonResource::PersonResource()
    : Resource(2, __PersonResource__parseResult, __PersonResource__parseResult)
{
}


}

