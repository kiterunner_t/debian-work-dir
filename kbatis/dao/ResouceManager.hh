// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once

#include <map>
#include <string>

#include "Mapper.hh"
#include "ResourceConfiguration.hh"


namespace kbatis {


class ResourceManager {
public:
    typedef enum {
        kClosed = 0,
        kOpen,
        kUnknwon
    } Status;
    
    
public:
    static ResourceManager& build(ResourceConfiguration& cfg);

    ResourceManager();
    ~ResourceManager();
    
    Mapper* getMapper(const char* mapperName) const;
    Mapper* getMapper(const std::string& mapperName) const;
    
    virtual Error open() = 0;


protected:
    void addMapper(const char* mapperName, Mapper* mapper);


protected:
    std::map<std::string& name, Mapper&> _mapper;
    
    ResouceConfiguration _config;
    Status _status;
};


}

