// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#progma once

#include <string>

#include <Error.h>
#include <Mapper.hh>
#include <MapResrouce.hh>
#include <VectorRerouce.hh>

#include "Person.hh"


class PersonMapper: public kbatis::Mapper {
public:
    // 需要自动生成一些构造函数的声明，或者说没有的话就给他生成一个。有的话就不生成了。
    // @TODO 构造函数中能调用自己的构造函数吗？
    // 是否需要在生成的代码中调用PersonMapper()构造函数
    // PersonMapper(ResourceManager* rm);

    ///@SQL insert person (id, name) values (#{_id}, #{_name});
    // insert person (id, name) values (?, ?);
    kbatis::Error store(IN Person& person);
    // void store(int id, std::string& name);

    ///@SQL select id, name from person where id=#{id};
    kbatis::Error getOne(IN int id, OUT Person& person);

    ///@SQL select id, name from person where id=#{id};
    kbatis::Error getOneBasic(IN int id, OUT int& idOut, OUT std::string& name);

    ///@SQL select id, name from person;
    kbatis::Error getAll(OUT kbatis::VectorResource<Person*>& persons);

    ///@SQL select id, name from person;
    kbatis::Error getAll(OUT kbatis::VectorResource<Person>& persons);

    ///@SQL select id, name from person;
    ///@KEY id
    kbatis::Error getAll(OUT kbatis::MapResource<int, Person>& persons);
};

