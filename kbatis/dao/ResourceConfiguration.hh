// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once

#include <string>

#include "Resouce.hh"


namespace kbatis {


class ResourceConfiguration {
public:
    ResourceConfiguration(const char* cfgFile);
    ResourceConfiguration(std::string& cfgFile);
    ResourceConfiguration(ResourceType type, const char* host, int port,
                          const char* dbName);
    ~ResourceConfiguration();
    
    const std::string& getHost() const;
    void setHost(const char* host);
    void setHost(const std::string& host);
    
    int getPort() const;
    void setPort(int port);
    
    const std::string& getUser() const;
    void setUser(const char* user);
    void setUser(const std::string& user);
    
    const std::string& getPassword() const;
    void setPassword(const char* password);
    void setPassword(const std::string& password);
    
    const std::string& getDbName() const;
    void setDbName(const char* name);
    void setDbName(std::string& name);
    
    ResourceType getType() const;


private:
    std::string _host;
    int _port;
    std::string _user;
    std::string _password;
    
    std::string _dbName;
    ResourceType _type;
};


}

