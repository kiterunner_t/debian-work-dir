// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once

namespace kbatis {


enum ResourceType {
    kRestJson = 0,
    kSqlite,
    kMySQL,
    kMax
};


// Just a flag for resource sub class
class Resource {
};


}

