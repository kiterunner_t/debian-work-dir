// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include "Resource.hh"
#include "ResourceConfiguration.hh"
#include "ResourceManager.hh"
#include "SqliteResouceManager.hh"


namespace kbatis {


// @TODO new一个ResourceManager，然后返回一个引用怎么做？
ResourceManager* ResourceManager::build(ResourceConfiguration& cfg)
{
    ResourceType type = cfg.getResourceType();
    ResourceManager* rm = NULL;
    switch (type) {
        case kSqlite:
            rm = new SqliteResourceManager(cfg);
            break;

        case kMySQL:
        case kRestJson:
        default:
            abort();  // @TODO
            break;
    }

    return rm;
}


}

