// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include <string>

#include "Mapper.hh"


namespace kbatis {


Mapper::Mapper(ResourceManager* rm, string& mapperName)
    : _resourceManager(rm),
      _mapperName(mapperName)
{
}


Mapper::~Mapper()
{
}


const std::string& Mapper::getMapperName() const
{
    return _mapperName;
}


}

