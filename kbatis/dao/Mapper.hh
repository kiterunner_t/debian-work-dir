// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once

#include <string>


namespace kbatis {


class Mapper {
public:
    Mapper(ResouceManager* rm);
    ~Mapper();
    
    const std::string& getMapperName() const;


private:
    Mapper(Mapper&);
    Mapper& operator =(Mapper&);


protected:
    ResouceManager* resourceManager;
    const std::string _mapperName;
};


}

