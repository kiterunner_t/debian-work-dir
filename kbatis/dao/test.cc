// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include "Person.hh"
#include "PersonMapper.hh"
#include "Resource.hh"
#include "ResourceConfiguration.hh"
#include "ResourceManager.hh"


using namespace kbatis;


int main()
{
    //ResourceConfiguration cfg("sqlite.cfg"); // 从配置文件中读取
    ResourceConfiguration cfg(ResourceType::kSqlite, "", 0, "test"); // 数据库类型
    ResourceManager& sqliteManager = ResourceManager::build(cfg);

    PersonMapper& personMapper = sqliteManager.getMapper("PersonMapper");

    // 事务支持

    Person person;
    personMapper.getOne(1, person);

    person.setName("change");
    personMapper.store(person);

    int id;
    string name;
    personMapper.getOneBasic(1, id, name);

    sqliteManager.close(); // 或者退出作用域的时候自动断开连接
    return 0;
}

