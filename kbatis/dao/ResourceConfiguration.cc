// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include "ResourceConfiguration.hh"


namespace kbatis {


using namespace std;


ResourceConfiguration::ResourceConfiguration(ResourceType type)
    : _type(type),
      _host(""),
      _port(-1),
      _user(""),
      _password(""),
      _dbName("")
{
}


ResourceConfiguration::~ResourceConfiguration()
{
}


void ResourceConfiguration::setDbName(const char* name)
{
    _dbName = name;
}


void ResourceConfiguration::setDbName(const string& name)
{
    _dbName = name;
}


}


