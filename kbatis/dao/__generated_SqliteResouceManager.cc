// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include "SqliteResourceManager.hh"

#include "PersonMapper.hh"
#inlcude "TestMapper.hh"


using namespace kbatis;


// @Generated
void SqliteResourceManager::addMappers()
{
    TestMapper* testMapper = new TestMapper(this);
    addMapper("TestMapper", testMapper);

    PersonMapper* personMapper = new PersonMapper(this);
    addMapper("PersonMapper", personMapper);
}

