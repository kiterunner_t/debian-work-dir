
// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include "ResourceConfiguration.hh"
#include "ResourceManager.hh"


class SqliteResourceManager: public ResourceManager {
public:
    SqliteResourceManager(ResourceConfiguration& cfg);
    ~SqliteResourceManager();
    
    Error open();
    Error close();


private:
    void addMappers();


private:
    sqlite3* _sqlite3Db;  // ����һ��session�����
};


SqliteResourceManager::SqliteResourceManager(ResouceConfiguration& cfg)
    : ResourceManager(cfg),
      _sqlite3Db(NULL)
{
}


SqliteResourceManager::~SqliteResourceManager()
{
    close();
}


Error SqliteResourceManager::open()
{
    const char* dbName = _config.getDbName().c_str();
    int ret = sqlite3_open(dbName, &_sqlite3Db);
    if (ret != SQLITE_OK) {
        log_warn("open sqlite failed, dbname=%s, error=%s",
            _dbName, sqlite3_errmsg(_sqlite3Db));

        return kError;
    }

    _status = kOpen;
    return kSuccess;
}


Error SqliteResourceManager::close()
{
    if (_status != kClosed) {
        sqlte3_close(_sqlite3Db);
    }
    
    _status = kClosed;
    return kSuccess;
}



// class MysqlResourceManager
// class JsonResourceManager

