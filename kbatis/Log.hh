// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#pragma once

#include <cassert>
#include <cstdarg>
#include <cstdio>
#include <string>


namespace krt {


class Log;
extern Log logger;


#define log_debug(fmt, ...) logger.log(Log::kDebug, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define log_info(fmt, ...)  logger.log(Log::kInfo, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define log_warn(fmt, ...)  logger.log(Log::kWarn, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define log_error(fmt, ...) logger.log(Log::kError, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define log_fatal(fmt, ...) logger.log(Log::kFatal, __FILE__, __LINE__, fmt, ##__VA_ARGS__)

#define log_set_level(level)       logger.setLevel(level)
#define log_set_filename(filename) logger.setFileName(filename)
#define log_to_stdout()            logger.toStdout()
#define log_close()                logger.close()


class Log {
public:
    typedef enum {
        kDebug = 0,
        kInfo,
        kWarn,
        kError,
        kFatal
    } Level;


public:
    Log();
    ~Log();

    void setLevel(const Log::Level level);
    void setFileName(const std::string& filename);
    void toStdout();
    void close();

    void log(Log::Level level, const char* fileName, int line, const char* fmt, ...);


private:
    static const int kMaxMsgLen = 1024;
    static const int kMaxLogSize = 100 * 1024 * 1024;

    Log::Level _level;
    std::string _fileName;
    std::string _path;
    FILE* _fp;
};


}

