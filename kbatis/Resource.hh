#pragma once

#include <string>
#include <vector>

#include <sqlite3.h>

#include "Error.hh"
    

namespace krt {


class Resource {
public:
    typedef void (*__ParseResult)(void*, Resource*);
    typedef void (*__ParseResultList)(void*, Resource**);


public:
    Resource(int fieldCount, __ParseResult parseResult, __ParseResultList parseResultList)
        : _fieldCount(fieldCount),
          _parseResult(parseResult),
          _parseResultList(parseResultList)
    {
    }

    virtual ~Resource() {}

    __ParseResult getParseResult() { return _parseResult; }
    __ParseResultList getParseResultList() { return _parseResultList; }
    int getFieldCount() { return _fieldCount; }
    
    
private:
    __ParseResult _parseResult;
    __ParseResultList _parseResultList;
    int _fieldCount;
};


template <typename T>
class ResourceVector {
public:
    typedef enum {
        kPointer = 0,
        kValue,
        kInvalid
    } Type;


public:
    Resource()
        : _type(kValue),
          _dummyPointer(&dummy)
    {
    }

    ResourceVector(Resource* resource)
        : _type(kPointer),
          _dummyPointer(resource)
    {
    }

    
    ~ResourceVector() {
        if (_type == kValue) {
            return ;
        }

        // delete _dummy;
        // @todo 指针类型，则释放；值类型，不做任何事
    }
    
    void add(T& r) { _resources.push_back(r); }
    T& get(int i) { return _resources[i]; }
    
    Resource* getDummy() { return _dummyPointer; }


private:
    std::vector<T> _resources;  // 根据类型来指定是指针还是赋值

    Type _type;
    T _dummy;
    Resource* _dummyPointer;
};


class ResourceVector {
public:
    ResourceVector() {}


    ~ResourceVector() {
        std::vector<Resource*>::iterator iter;
        for (iter = _resources.begin(); iter != _resources.end(); ++iter) {
            delete *iter;
        }

        _resources.clear();
    }
    
    void add(Resource* r) { _resources.push_back(r); }
    Resource* get(int i) { return _resources[i]; }


private:
    std::vector<Resource*> _resources;
};


class ResourceManager {
public:
    ResourceManager() {}
    ~ResourceManager() {}

    virtual ErrorCode login() { return kSuccess; }
    virtual ErrorCode logout() { return kSuccess; }

    virtual ErrorCode get(const std::string& sql, Resource& r, ResourceVector& result) { return kSuccess; }
    virtual ErrorCode get(const std::string& sql, Resource& result) { return kSuccess; }
    
    virtual ErrorCode get(const std::string& sql, void* parsed) { return kSuccess; }
};


}

