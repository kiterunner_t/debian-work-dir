#pragma once

#include <string>

#include <sqlite3.h>

#include "Error.hh"
#include "Resource.hh"


namespace krt {
    
    
class Sqlite3ResourceManager: public ResourceManager {
public:
    Sqlite3ResourceManager(std::string& name);
    ~Sqlite3ResourceManager();
    
    
    virtual ErrorCode login();
    virtual ErrorCode logout();

    virtual ErrorCode get(const std::string& sql, Resource& r, ResourceVector& result);
    virtual ErrorCode get(const std::string& sql, Resource& result);
    
    virtual ErrorCode get(const std::string& sql, void* parsed);


private:
    std::string _dbName;
    bool _isOpen;

    sqlite3* _sqlite3Db;
};


}

