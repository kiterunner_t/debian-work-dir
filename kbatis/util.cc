// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include <string>

#include <iconv.h>

#include "Error.hh"
#include "Log.hh"
#include "util.hh"


namespace krt {


using namespace std;


static ErrorCode _convert(const string& from, const string& to,
                          string& buf, int tmpSize);


static const string kGbk = "GBK";
static const string kUtf8 = "UTF-8";
static const int kMaxTmpArraySize = 8 * 1024;
static const int kMaxConvertStringSize = 2 * 1024 * 1024;


ErrorCode gbk_to_utf8(string& buf)
{
    return _convert(kGbk, kUtf8, buf, (buf.size() * 2) + 1);
}


ErrorCode utf8_to_gbk(string& buf)
{
    return _convert(kUtf8, kGbk, buf, buf.size() + 1);
}


// if convert failed, do nothing.
static ErrorCode _convert(const string& fromCode, const string& toCode, string& buf, int tmpSize)
{
    if (tmpSize > kMaxConvertStringSize) {
        log_info("buf is too long, so do not convert it, size=%d", tmpSize);
        return kErrorExceed;
    }

    if (tmpSize < 2) {
        return kSuccess;
    }

    iconv_t conv = iconv_open(toCode.c_str(), fromCode.c_str());
    if (conv == NULL) {
        log_error("_convert open iconv error");
        return kError;
    }

    // 存在两个坑：
    // 1) VC中，iconv修改了toPtr，导致后续的delete等操作crash，因此
    //    使用两个指针，一个用于iconv修改，一个用于delete等后续操作。
    // 2) 返回的tSize似乎不是字节大小，因此缓冲区都memset了一把。
    char* toPtr;
    char* p;
    if (tmpSize > kMaxTmpArraySize) {
        p = new char[tmpSize];
        toPtr = p;
        memset(p, 0, tmpSize);

    } else {
        static char t[kMaxTmpArraySize];
        
        p = t;
        toPtr = p;
        memset(p, 0, kMaxTmpArraySize);
    }

    size_t tSize = static_cast<size_t>(tmpSize);
    ErrorCode err = kSuccess;

    const char* from = buf.c_str();
    size_t fromSize = static_cast<size_t>(buf.size());

    int ret = iconv(conv, &from, &fromSize, &toPtr, &tSize);
    if (ret == -1) {
        log_error("iconv failed, string: \"%s\", from: %s, to: %s",
                  buf.c_str(), fromCode.c_str(), toCode.c_str());
        err = kError;

    } else {
        string s(p);
        buf.swap(s);
    }

    if (tmpSize > kMaxTmpArraySize) {
        delete[] p;
    }

    iconv_close(conv);
    return err;
}


}

