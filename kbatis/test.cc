// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include <iostream>
#include <string>

#include "Error.hh"
#include "Log.hh"
#include "PersonResource.hh"
#include "Resource.hh"
#include "Sqlite3ResourceManager.hh"


using namespace std;
using namespace krt;


/*

http://www.tuicool.com/articles/jIZr2q2
http://generator.sturgeon.mopaas.com/index.html
*/

int main()
{
    log_to_stdout();

    string name = "test.db";
    ResourceManager* sqlite = new Sqlite3ResourceManager(name);
    sqlite->login();

    string sql = "select id, name from person";
    PersonResource person;
    ErrorCode ret = sqlite->get(sql, person);
    if (ret != kSuccess) {
        cout << "fetch failed" << endl;
        delete sqlite;
        return 0;
    }

    cout << person._id << " " << person._name << endl;
    
    ResourceVector vec;
    ret = sqlite->get(sql, person, vec);
    if (ret != kSuccess) {
        cout << "fetch failed in batch" << endl;
        delete sqlite;
        return 0;
    }

    PersonResource* p = static_cast<PersonResource*>(vec.get(0));
    cout << p->_id << " " << p->_name << endl;
    

    ResourceVector<PersonResource> personVecValue;
    ResourceVector<PersonResource*> personVecPt(new PersonResource);

    ret = sqlite->get(sql, personVecValue);  // ģ���ֲ���virtual��
    ret = sqlite->get(sql, personVecPt);
    
    PersonResource& p0 = personVecValue.get(0);
    cout << p0._id << p0._name << endl;
    
    PersonResource*& pp0 = personVecPt.get(0);
    cout << pp0->_id << pp0->_name << endl;
    
    delete sqlite;
    return 0;
}

