// Copyright (C) KRT, 2016 by kiterunner_t
// TO THE HAPPY FEW

#include <string>

#include <sqlite3.h>

#include "Error.hh"
#include "Log.hh"
#include "Sqlite3ResourceManager.hh"


namespace krt {


Sqlite3ResourceManager::Sqlite3ResourceManager(std::string& name)
    : _sqlite3Db(NULL),
      _dbName(name),
      _isOpen(false)
{
}


Sqlite3ResourceManager::~Sqlite3ResourceManager()
{
    logout();
}


ErrorCode Sqlite3ResourceManager::login()
{
    // @todo 看看这些函数的错误码
    int ret = sqlite3_open(_dbName.c_str(), &_sqlite3Db);
    if (ret != SQLITE_OK) {
        log_warn("open sqlite failed, dbname=%s, error=%s",
            _dbName.c_str(), sqlite3_errmsg(_sqlite3Db));

        return kError;
    }

    _isOpen = true;
    return kSuccess;
}


ErrorCode Sqlite3ResourceManager::logout()
{
    if (_isOpen == true) {
        sqlite3_close(_sqlite3Db);
    }
    
    _isOpen = false;
    return kSuccess;
}


ErrorCode Sqlite3ResourceManager::get(const std::string& sql, Resource& parsed)
{
    sqlite3_stmt* result;
    const char* s = NULL;
    int rc = sqlite3_prepare(_sqlite3Db, sql.c_str(), sql.length(), &result, &s);
    if (rc != SQLITE_OK) {
        log_warn("fetch error, %s, sql=%s", sqlite3_errmsg(_sqlite3Db), sql.c_str());
        return kErrorFetch;
    }

    int count = sqlite3_column_count(result);
    if (count < 0) {
        log_info("there is no result");
        return kErrorNoData;
    }
    
    int resourceFieldCount = parsed.getFieldCount();
    if (count != resourceFieldCount) {
        return kErrorNotMatch;
    }
    
    rc = sqlite3_step(result);
    if (rc == SQLITE_BUSY || rc==SQLITE_ERROR || rc==SQLITE_MISUSE) {
        sqlite3_finalize(result);
        return kErrorFetch;
    }
    
    if (rc == SQLITE_DONE) {
        return kErrorNoData;
    }
    
    Resource::__ParseResult parseResult = parsed.getParseResult();
    parseResult(result, &parsed);

    sqlite3_finalize(result);
    return kSuccess;
}


ErrorCode Sqlite3ResourceManager::get(const std::string& sql, Resource& r, ResourceVector& parsed)
{
    sqlite3_stmt* result;
    const char* s = NULL;
    int rc = sqlite3_prepare(_sqlite3Db, sql.c_str(), sql.length(), &result, &s);
    if (rc != SQLITE_OK) {
        log_warn("fetch error");
        return kErrorFetch;
    }

    int count = sqlite3_column_count(result);
    if (count < 0) {
        log_info("there is no result");
        return kErrorNoData;
    }
    
    ErrorCode ret = kSuccess;
    while (true) {
        rc = sqlite3_step(result);
        if (rc == SQLITE_DONE) {
            break;
        }

        if (rc == SQLITE_BUSY || rc==SQLITE_ERROR || rc==SQLITE_MISUSE) {
            sqlite3_finalize(result);
            ret = kErrorFetch;
            break;
        }

        Resource* p = NULL;
        Resource::__ParseResultList parseResultList = r.getParseResultList();
        parseResultList(result, &p);

        parsed.add(p);
    }
    
    sqlite3_finalize(result);
    return ret;
}



ErrorCode Sqlite3ResourceManager::get(const std::string& sql, void* out)
{
    ResourceVectorT<Resource>* parsed = static_cast<ResourceVectorT<Resource>* >(out);

    sqlite3_stmt* result;
    const char* s = NULL;
    int rc = sqlite3_prepare(_sqlite3Db, sql.c_str(), sql.length(), &result, &s);
    if (rc != SQLITE_OK) {
        log_warn("fetch error");
        return kErrorFetch;
    }

    int count = sqlite3_column_count(result);
    if (count < 0) {
        log_info("there is no result");
        return kErrorNoData;
    }
    
    ErrorCode ret = kSuccess;
    while (true) {
        rc = sqlite3_step(result);
        if (rc == SQLITE_DONE) {
            break;
        }

        if (rc == SQLITE_BUSY || rc==SQLITE_ERROR || rc==SQLITE_MISUSE) {
            sqlite3_finalize(result);
            ret = kErrorFetch;
            break;
        }

        Resource* p = NULL;
        Resource* dummy = parsed->getDummy();
        
        Resource::__ParseResultList parseResultList = dummy->getParseResultList();
        parseResultList(result, &p);

        parsed->add(p);
    }
    
    sqlite3_finalize(result);
    return ret;
}


}

