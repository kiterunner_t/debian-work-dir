#include <string>

#include "Error.hh"


namespace krt {


ErrorCode gbk_to_utf8(std::string& out);
ErrorCode utf8_to_gbk(std::string& out);


}

