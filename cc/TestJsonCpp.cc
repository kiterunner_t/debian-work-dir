#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <iostream>

#include <jsoncpp/json/json.h>

using namespace std;
using namespace Json;


int main()
{ 
    Json::FastWriter writer;
    Json::Reader reader;

    Json::Value map;
    map["name"] = "中文";
    map["age"] = 23;

    string jsonData = writer.write(map);
    cout << jsonData << endl;

    Json::Value parseData;
    jsonData = "{\"name\":\"test\", \"age\":23}";
    reader.parse(jsonData, parseData);

    Json::Value valueName = "默认";
    Json::Value valueAge = -1;
    valueName = parseData.get("name", valueName);
    valueAge = parseData.get("age", valueAge);

    const char* nameStr = valueName.asCString() ;
    int age = valueAge.asInt();
    printf("name:%s,age:%d\n", nameStr, age);
    
    
    // --
    string jsonStr = "{\
\"object\":\"role\",\
\"data\":[\
{\"roleId\":\"000\",\"roleName\":\"测试角色\",\"roleFlag\":\"MM\"},\
{\"roleName\":\"副总经理\",\"roleFlag\":\"DGM\"}\
]\
}";
    Json::Value root;
    bool ret = reader.parse(jsonStr, root);
    printf("object: %s\n", root.get("object", "<>").asString().c_str());
    
    Json::Value data = root.get("data", -1);
    if (data == -1) {
        printf("data is -1\n");
    }
    
    Json::Value v = data[0];
    Json::Value::Members members(v.getMemberNames());
    
    int dataSize = data.size();
    for (int i = 0; i < dataSize; ++i) {
        printf("--------------\n");
        for (Json::Value::Members::iterator it = members.begin(); it != members.end(); ++it) {
            const string& name = *it;
            Json::Value v = data[i].get(name, -1);
            if (v == -1) {
                printf("v data is -1\n");
            }
            
            printf("key: %s, value: %s\n", name.c_str(), v.asString().c_str());
        }
        
        printf("\n");
    }

    return 0;
}

