#include <spdlog/spdlog.h>

#include <string>
#include <iostream>

// g++ -I ./spdlog-0.11.0/include -std=c++11 TestSpdlog.cc

int main()
{
    namespace spd = spdlog;
    
    std::string filename = "spdlog_example";
    auto file_logger = spd::rotating_logger_mt("file_logger", filename, 1024 * 1024 * 5, 3);
    file_logger->info("Log file message number", 1);

    for (int i = 0; i < 100; ++i) {
        auto square = i*i;
     //   file_logger->info() << i << '*' << i << '=' << square << " (" << "0x" << std::hex << square << ")";
    }

}

