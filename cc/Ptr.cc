#include <iostream>
#include <memory>


using namespace std;


class Test {
public:
    Test() { cout << "Test()" << endl; }
    ~Test() { cout << "~Test" << endl; }

    void p() { cout << "hello" << endl; }
};


int main(void)
{
    shared_ptr<Test> p = make_shared<Test>();
    p->p();

    auto q = p;
    q->p();
    
    return 0;
}

