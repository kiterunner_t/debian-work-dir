#include <iostream>


template <typename T>
void test2(T l)
{
    std::cout << l << std::endl;
}


template <typename T>
void test(T l)
{
    test2(l);
}


int main()
{
    int a = 1;
    long long b = 1;
    const char* c = "abc";
    std::string d = "def";

    test(1);
    test(a);
    test(b);
    test(c);
    test(d);
    return 0;
}

