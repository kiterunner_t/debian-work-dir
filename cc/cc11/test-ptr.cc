#include <iostream>
#include <memory>


using namespace std;


void foo(int* p)
{
    cout << *p << endl;
}


int main()
{
    unique_ptr<int> p1(new int(42));
    unique_ptr<int> p2 = std::move(p1);
    
    if (p1)
        foo(p1.get());
    
    (*p2)++;
    
    if (p2)
        foo(p2.get());
    
    return 0;
}
