// #program once

#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <vector>


class Value {
public:
    Value(bool b);
    Value(int i);
    Value(const char* v);
    Value(std::string* v);
    Value(std::vector<Value>* v);
    
    // ~Value();

    bool getBool();
    int getInt();
    std::string* getString();
    std::vector<Value>* getArray();


private:
    enum ValueType {
        kBool = 0,
        kInt,
        kChar,
        kString,
        kArray,
        kMap
    };
    
    
    union ValueInternal {
        bool  b;
        int   i;
        void* p;
        
        ValueInternal(bool v): b(v) {}
        ValueInternal(int v): i(v) {}
        ValueInternal(char* v): p(v) {}
        ValueInternal(std::string* v): p(v) {}
        ValueInternal(std::vector<Value>* v): p(v) {}
        ValueInternal(std::map<std::string, Value>* v): p(v) {}
    };
    
    
    enum ValueType _type;
    union ValueInternal _value;
};


Value::Value(bool v)
    : _type(kBool),
      _value(v)
{
}


Value::Value(int v)
    : _type(kInt),
      _value(v)
{
}


Value::Value(const char* v)
    : _type(kChar),
      _value(new std::string(v))  // �ڴ�й©
{
}


Value::Value(std::string* v)
    : _type(kString),
      _value(v)
{
}


Value::Value(std::vector<Value>* v)
    : _type(kArray),
      _value(v)
{
}


bool Value::getBool()
{
    assert(_type == kBool);
    return _value.b;
}


int Value::getInt()
{
    assert(_type == kInt);
    return _value.i;
}


std::string* Value::getString()
{
    std::cout << _type << std::endl;
    assert(_type == kString || _type == kChar);
    
    return static_cast<std::string*>(_value.p);
}


std::vector<Value>* Value::getArray()
{
    assert(_type == kArray);
    return static_cast<std::vector<Value>*>(_value.p);
}


int main()
{
    Value bv(true);
    bool b = bv.getBool();
    
    Value iv(9999);
    int i = iv.getInt();
    
    std::string ss("hello");
    Value sv(&ss);
    std::string* s = sv.getString();
    
    Value v1(999);
    //std::string world("world");
    Value v2("world");
    std::vector<Value> arr;
    arr.push_back(v1);
    arr.push_back(v2);
    
    Value vv(&arr);
    std::vector<Value>* arr2 = vv.getArray();
    Value v1v = (*arr2)[0];
    int v1i = v1v.getInt();
    
    Value v2v = (*arr2)[1];
    std::string* v2s = v2v.getString();

    std::cout << b << std::endl;
    std::cout << i << std::endl;
    std::cout << *s << std::endl;
    
    std::cout << v1i << std::endl;
    std::cout << *v2s << std::endl;
    return 0;
}

