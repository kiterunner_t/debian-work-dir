#include <stdio.h>


class Object {
public:
    static void memLeakCheck() {
        printf("count: %d\n", _count);
    }


    Object() {
        _count++;
        printf("object init\n");
    }


    ~Object() {
        _count--;
    }



private:
    static __thread long _count;
};


__thread long Object::_count = 0;


class SubObj1: Object {
public:
    
private:
    int _a;
};


class SubObj2: Object {
public:
    SubObj2(): _b(1) {}

    
private:
    int _b;
};


int main(void)
{
    SubObj1* obj1 = new SubObj1();
    SubObj1* obj1_2 = new SubObj1();

    delete obj1;
    delete obj1_2;

    SubObj2* obj2 = new SubObj2();
    delete obj2;
    Object::memLeakCheck();
}

