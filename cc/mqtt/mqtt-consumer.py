#! /usr/bin/env python
# coding: utf-8

import os
import sys
import time

import paho.mqtt.client as mqtt



queue_name = "test-topic"
client_id = "mqtt-test-consumer"



ip = "192.168.239.137"
port = 1883
user = "test"
passwd = "test"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(queue_name, qos=1)


def on_message(client, userdata, msg):
    global count
    count = count + 1
    body = str(msg.payload)
    print body

    if body == "shutdown":
        global start
        end = time.time()
        diff = end - start
        if diff == 0:
            diff = 0.001

        print "time: ", diff
        print "msg count: ", count + 1
        print "count per sec: ", count / diff
        client.loop_stop()


client = mqtt.Client(client_id=client_id, clean_session=0)
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(user, password=passwd)
client.connect(ip, port, 60)

start = time.time()
count = 0

    
client.loop_forever()
print "xxxxx"
client.disconnect()

