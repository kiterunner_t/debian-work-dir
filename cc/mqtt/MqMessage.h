#pragma once

#include <string>


namespace rt {


typedef enum {
    kQosAtMostOnce = 0,
    kQosAtLeastOnce,
    kQosExactly
} Qos;


class MqMessage {
public:
    MqMessage() {}
    ~MqMessage() {}

    std::string& getTopic() { return _topic; }
    void setTopic(const char* topic) { _topic = topic; }

    std::string& getBody() { return _body; }
    void setBody(const char* body) { _body = body; }
    
    Qos getQos() { return _qos; }
    void setQos(Qos qos) { _qos = qos; }


private:
    std::string _topic;
    std::string _body;
    Qos _qos;
};


}

