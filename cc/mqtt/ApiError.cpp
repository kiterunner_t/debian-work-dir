#include "ApiError.h"

using namespace std;


const string CApiError::astrApiErrorMessage[] = {
    "success",
    "api common error",
    "api request enqueue",
    "connection error, offline maybe",
    "server time out",
    "http request failed, please refer to http status code for more information"
};


const string& CApiError::getErrorMessage(int code)
{
    static const string strTemp = "";
    if (code > API_SUCCESS && code < API_ERROR_MAX) {
        return astrApiErrorMessage[code];
    }

    return strTemp;
}

