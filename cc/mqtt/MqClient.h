#pragma once

#include <string>
#include <vector>

#include <MQTTClient.h>
#include <rapidjson/document.h>

#include "ApiError.h"
#include "Log.h"
#include "MqMessage.h"


namespace rt {
    

class MqClient {
public:
    MqClient(std::string& clientId, std::string& ip, int port);
    ~MqClient();

    ApiErrorCode init();
    
    ApiErrorCode connect();
    ApiErrorCode disconnect();

    ApiErrorCode publish(std::string& topic, std::string& body, Qos qos,
                         unsigned long timeout = kDefaultTimeout);

    // ApiErrorCode subscribe(std::string& topic);
    // ApiErrorCode receive(MqMessage& msg, unsigned long timeout = kDefaultTimeout);

    void setKeepAlive(int keepAlive);


private:
    MQTTClient_connectOptions _initConnectOption();
    const char* _getMqttError(int errorCode);
    const char* _getMqttConnectError(int ret);

    MqClient(const MqClient&);
    MqClient& operator =(const MqClient&);
    

private:
    static const int kDefaultKeepAlive = 60;
    static const int kDefaultTimeout = 1;
    static const int kDefaultReconnectInterval = 30;
    static const int kMaxReconnectInterval = 32 * 60;

    static const int kDefaultPort = 1883;

    // ClientIDs must be no longer than 23 chars according to the MQTT spec
    static const int kClientIdMaxSize = 23;

    MQTTClient _mqtt;
    MQTTClient_connectOptions _connectOption;

    std::string _clientId;
    std::string _ip;
    int _port;

    int _keepAlive;
    bool _isConnected;
};


MqClient::MqClient(std::string& clientId, std::string& ip, int port)
    : _clientId(clientId),
      _ip(ip),
      _port(port),
      _keepAlive(kDefaultKeepAlive),
      _isConnected(false),
      _connectOption(_initConnectOption())
{
    _connectOption.cleansession = 0;
    _connectOption.keepAliveInterval = _keepAlive;
}


MQTTClient_connectOptions MqClient::_initConnectOption()
{
    return MQTTClient_connectOptions_initializer;
}


MqClient::~MqClient()
{
    disconnect();
}


ApiErrorCode MqClient::init()
{
    if (_clientId.size() > kClientIdMaxSize) {
        log_error("ClientId is too long, client id=%s", _clientId.c_str());
        return API_ERROR_FAILED;
    }

    char portStr[32];
    snprintf(portStr, sizeof(portStr), "%d", _port);
    
    std::string addr = "tcp://" + _ip + ":";
    addr += portStr;

    int ret;
    ret = MQTTClient_create(&_mqtt, addr.c_str(), _clientId.c_str(), 
                            MQTTCLIENT_PERSISTENCE_NONE, NULL);
    if (ret != MQTTCLIENT_SUCCESS) {
        log_warn("Mqtt client create error, error_code=%d", ret);
        return API_ERROR_FAILED;
    }

    return API_SUCCESS;
}


ApiErrorCode MqClient::connect()
{
    if (_isConnected == true) {
        return API_SUCCESS;
    }
    
    int ret = MQTTClient_connect(_mqtt, &_connectOption);
    if (ret != MQTTCLIENT_SUCCESS) {
        log_warn("MQTT connect error, %s", _getMqttConnectError(ret));
        return API_ERROR_FAILED;
    }

    _isConnected = true;
    return API_SUCCESS;
}


ApiErrorCode MqClient::disconnect()
{
    if (_isConnected == true) {
         MQTTClient_disconnect(_mqtt, 10000);
        _isConnected = false;
    }
}


ApiErrorCode MqClient::receive(MqMessage& msg, unsigned long timeout)
{
    int ret;
    char* topicName = NULL;
    int topicLen = 0;
    MQTTClient_message* receivedBody = NULL;

    ret = MQTTClient_receive(_mqtt, &topicName, &topicLen, &receivedBody, timeout);
    if (ret != MQTTCLIENT_SUCCESS) {
        log_warn("mqtt client receive exception, errorCode=%d", ret);
        return API_ERROR_FAILED;
    }

    msg.setTopic(topicName);
    msg.setBody(static_cast<char*>(receivedBody->payload));

    MQTTClient_free(topicName);
    MQTTClient_freeMessage(&receivedBody);
    return API_SUCCESS;
}


ApiErrorCode MqClient::publish(std::string& topic, std::string& body, 
                               Qos qos, unsigned long timeout)
{
    if (_isConnected==false && connect()!=API_SUCCESS) {
        return API_ERROR;
    }
    
    log_debug("pub topic=\"%s\", body=\"%s\"", topic.c_str(), body.c_str());

    MQTTClient_message pubMsg = MQTTClient_message_initializer;
    
    char* msgBody = new char[body.size()];
    memcpy(msgBody, body.c_str(), body.size());
    pubMsg.payload = static_cast<void*>(msgBody);
    pubMsg.payloadlen = body.size();
    pubMsg.qos = qos;
    pubMsg.retained = 0;
    
    int ret = MQTTClient_publishMessage(_mqtt, topic.c_str(), &pubMsg, NULL);
    delete[] msgBody;

    if (ret !=  MQTTCLIENT_SUCCESS ) {
        log_warn("MQTT publish error, error message=%s", _getMqttError(ret));

        if (ret != MQTTCLIENT_DISCONNECTED) {
            _isConnected = false;
            return API_ERROR_OFFLINE;
        }

        return API_ERROR_FAILED;
    }

    MQTTClient_deliveryToken token;
    ret = MQTTClient_waitForCompletion(_mqtt, token, timeout);

    switch (ret) {
        case MQTTCLIENT_SUCCESS:
            return API_SUCCESS;

        case MQTTCLIENT_DISCONNECTED:
            log_warn("MQTT publish error, error message=%s", _getMqttError(ret));
            _isConnected = false;
            return API_ERROR_OFFLINE;

        default:
            log_warn("MQTT publish error, error message=%s", _getMqttError(ret));
            return API_ERROR_FAILED;
    }
}


const char* MqClient::_getMqttError(int errorCode)
{
    static const char* mqttError[] = {
        "Success",
        "A generic error code indicating the failure of an MQTT client operation",
        "Unknown error",
        "The client is disconnected",
        "The maximum number of messages allowed to be simultaneously in-flight has been reached",
        "An invalid UTF-8 string has been detected",
        "A NULL parameter has been supplied when this is invalid",
        "The topic has been truncated (the topic string includes embedded NULL characters)",
        "A structure parameter does not have the correct eyecatcher and version number",
        "A QoS value that falls outside of the acceptable range (0,1,2)",
        "Unknown error"
    };
    
    const char* errorMsg = mqttError[10];
    if (errorCode>-10 && errorCode<1) {
        int n = -errorCode;
        assert(n>=0 && n<=9);
        errorMsg = mqttError[n];
    }
    
    return errorMsg;
}


const char* MqClient::_getMqttConnectError(int ret)
{
    static const char* connectError[] = {
        "",
        "Connection refused: Unacceptable protocol version",
        "Connection refused: Identifier rejected",
        "Connection refused: Server unavailable",
        "Connection refused: Bad user name or password",
        "Connection refused: Not authorized",
        "Unknown error"
    };

    const char* errorMsg = connectError[6];
    if (ret > 0 && ret < 6) {
        errorMsg = connectError[ret];
    }
    
    return errorMsg;
}


}

