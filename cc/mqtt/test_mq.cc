#include "ApiError.h"
#include "Log.h"
#include "MqClient.h"
#include "MqMessage.h"

#include <unistd.h>

#include <cstdio>


int main()
{
    std::string clientId = "test-client-id";
    std::string ip = "localhost";
    rt::MqClient mq(clientId, ip, 1883);
    mq.init();

    if (mq.connect() != 0) {
        printf("error connect\n");
        return 1;
    }

    std::string t = "xxx";
    
    std::string topic = "test-topic";
    
    int count = 0;
    while (1) {
        char countStr[32];
        snprintf(countStr, sizeof(countStr), "-%d", count++);
        std::string tt = t + countStr;
        
        ApiErrorCode ret = mq.publish(topic, tt, rt::kQosAtLeastOnce, 1000);
        if (ret == API_ERROR_OFFLINE) {
            printf("offline -> ");
        } else if (ret != API_SUCCESS) {
            printf("other error ");
        }

        printf("--\n");
        sleep(2);
    }
}

