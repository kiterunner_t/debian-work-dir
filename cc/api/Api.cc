#include <string>

#include "eutils_arm_v2.h"
#include "Resource.hh"

using namespace std;


Resource::Resource()
{
    _transport.init();
}


Resource::~Resource()
{
}


void Resource::setModuleName(string& moduleName)
{
    _getRequest.setModuleName(moduleName);
    _modifyRequest.setModuleName(moduleName);
}


void Resource::setResourceName(string& resourceName)
{
    _getRequest.setResourceName(resourceName);
    _modifyRequest.setResourceName(resourceName);
}

 
bool Resource::get(CSelectHelp& help)
{
    bool ret = _transport.doGet(_getRequest, _response);
    if (ret == false || _response.checkStatus() == false) {
        _getRequest.clear();
        return false;
    }

    if (help.fromJsonString(_response.getBody()) < 0) {
        _getRequest.clear();
        return false;
    }
    
    _getRequest.clear();
    return true;
}


bool Resource::post()
{
    _modifyRequest.setType(kPost);
    bool ret = _transport.doPost(_modifyRequest, _response);
    if (ret == false || _response.checkStatus() == false) {
        _modifyRequest.clear();
        return false;
    }

    return true;
}


bool Resource::put()
{
    _modifyRequest.setType(kPut);
    bool ret = _transport.doPut(_modifyRequest, _response);
    if (ret == false || _response.checkStatus() == false) {
        _modifyRequest.clear();
        return false;
    }

    return true;
}


bool Resource::del()
{
    _modifyRequest.setType(kDel);
    bool ret = _transport.doDel(_modifyRequest, _response);
    if (ret == false || _response.checkStatus() == false) {
        _modifyRequest.clear();
        return false;
    }

    return true;
}


bool Resource::batch()
{
    _modifyRequest.setType(kBatch);
    bool ret = _transport.doPost(_modifyRequest, _response);
    if (ret == false || _response.checkStatus() == false) {
        _modifyRequest.clear();
        return false;
    }

    return true;
}

