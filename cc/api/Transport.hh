#pragma once

#include "Request.hh"
#include "Response.hh"


class Transport {
public:
    virtual bool init() = 0;

    virtual bool doGet(Request& request, Response& response) = 0;
    virtual bool doPost(Request& request, Response& response) = 0;
    virtual bool doPut(Request& request, Response& response) = 0;
    virtual bool doDel(Request& request, Response& response) = 0;
};

