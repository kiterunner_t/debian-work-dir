#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <ctime>

#include <pthread.h>

#include "Log.hh"

using namespace std;


Log logger;
static pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;


Log::Log()
    : _level(Log::kWarn),
      _path("./"),
      _fileName("RT.log"),
      _fp(NULL)
{
}


Log::~Log()
{
    if (_fp != NULL && _fp != stdout) {
        fclose(_fp);
    }
}


void Log::setLevel(const Log::Level level)
{
    _level = level;
}


void Log::setFileName(const string& filename)
{
    _fileName = filename;
    close();
}


void Log::toStdout()
{
    if (pthread_mutex_lock(&g_mutex) != 0) {
        return ;
    }

    if (_fp != NULL) {
        fclose(_fp);
    }

    _fp = stdout;

    pthread_mutex_unlock(&g_mutex);
}


void Log::close()
{
    if (pthread_mutex_lock(&g_mutex) != 0) {
        return ;
    }

    if (_fp != NULL && _fp != stdout) {
        fclose(_fp);
        _fp = NULL;
    }

    pthread_mutex_unlock(&g_mutex);
}


void Log::log(Log::Level level, const char* fileName, int line, const char* fmt, ...)
{
    static const char* logLevelName[] = {"DEBUG", "INFO", "WARN", "ERROR"};
    
    if (level < _level) {
        return ;
    }

    char msg[kMaxMsgLen];
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(msg, sizeof(msg), fmt, ap);
    va_end(ap);

    time_t t = time(NULL);
    struct tm result;
    localtime_r(&t, &result);
    char timeStr[32];
    timeStr[0] = 0;
    strftime(timeStr, sizeof(timeStr), "%Y-%m-%d %H:%M:%S", &result);

    const char* levelStr = logLevelName[level];

    //    if (pthread_mutex_lock(&g_mutex) != 0) {
    //  return ;
    //}

    if (_fp == NULL) {
        string file = _path + _fileName;
        _fp = fopen(file.c_str(), "a");
    }

    if (_fp == NULL) {
        pthread_mutex_unlock(&g_mutex);
        return ;
    }

    char buf2[2048];
    sprintf(buf2, "[%s %s] [%s/%d]: %s\n", levelStr, timeStr, fileName, line, msg);

    write(fileno(_fp), buf2, strlen(buf2));

    if (level == Log::kFatal) {
        fprintf(_fp, "[%s %s] [%s/%d]: abort\n", levelStr, timeStr, fileName, line, msg);
        abort();
    }

    //    pthread_mutex_unlock(&g_mutex);
}

