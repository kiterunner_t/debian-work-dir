#pragma once

#include <curl/curl.h>

#include "Transport.hh"


class HttpTransport: public Transport {
public:
    HttpTransport(): _isInit(false), _isDebug(false) {}
    ~HttpTransport() {}


    bool init() {
	    if (_isInit == false) {
	        curl_global_init(CURL_GLOBAL_ALL);
	        _isInit = true;
	    }

	    return true;
	}


	bool doGet(Request& request, Response& response) { return _do("GET", request, response); }
	bool doPut(Request& request, Response& response) { return _do("PUT", request, response); }
	bool doPost(Request& request, Response& response) { return _do("POST", request, response); }
	bool doDel(Request& request, Response& response) { return _do("DEL", request, response); }


private:
    bool _do(const char* method, Request& request, Response& response);


private:
    bool _isInit;
    bool _isDebug;
};

