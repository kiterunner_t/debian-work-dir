#include <cstdio>
#include <cstdlib>
#include <string>

#include <curl/curl.h>

#include "HttpTransport.hh"
#include "Log.hh"
#include "Transport.hh"

using namespace std;


static size_t _write(void* buf, size_t size, size_t nmemb, void* out)
{
    string* s = static_cast<string*>(out);
    if (s == NULL || buf == NULL) {
        return -1;
    }
 
    char* data = static_cast<char*>(buf);
    size_t totalSize = size * nmemb;
    s->append(data, totalSize);
    return totalSize;
}


bool HttpTransport::_do(const char* method, Request& request, Response& response)
{
    response.clear();

    CURL* curl = curl_easy_init();
    if (curl == NULL) {
        log_error("curl init error");
        return false;
    }
    
    CURLcode ret;
    curl_slist* httpHeader = NULL;
    httpHeader = curl_slist_append(httpHeader, "Accept: application/json");
    httpHeader = curl_slist_append(httpHeader, "Content-Type: application/json");
    httpHeader = curl_slist_append(httpHeader, "charsets: utf-8");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, httpHeader);

    if (HttpTransport::_isDebug) {
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
        log_debug("transport debug level");
    }

    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 1);

    string responseStr;
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*) &responseStr);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ::_write);

    const string& url = request.getUrl();
    printf("url: %s\n", url.c_str());
    bool retCode = true;
    ret = curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    if (ret != CURLE_OK) {
        log_error("failed set transfer url: %s, error: %d", url.c_str(), ret);
        curl_slist_free_all(httpHeader);
        curl_easy_cleanup(curl);
        return false;
    }
    
    const string& body = request.getBody();
    printf("body: %s\n", body.c_str());
    ret = curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body.c_str());
    if (ret != CURLE_OK) {
        log_error("failed set transfer body: %s, error: %d", body.c_str(), ret);
        curl_slist_free_all(httpHeader);
        curl_easy_cleanup(curl);
        return false;
    }

    // @todo ����response�������ֶ�
    ret = curl_easy_perform(curl);
    if (ret != CURLE_OK) {
        log_error("error");
        curl_slist_free_all(httpHeader);
        curl_easy_cleanup(curl);
        return false;
    }

    response.setRequest(&request);
    response.setCode(kResponseOk);
    response.setBody(responseStr);

    curl_slist_free_all(httpHeader);
    curl_easy_cleanup(curl);
    return true;
}

