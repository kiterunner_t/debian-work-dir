#pragma once

#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint;

//常量定义区
const char* const CONST_EUTILS_VERSION = "1.1";
//字段间的分隔符
const char  CONST_SEP_FIELD[] = {0x01,'\0'};
//行间的分隔符
const char  CONST_SEP_LINE[] = {0x02,'\0'};
//行行分隔符
const char  CONST_SEP_LINE_LINE[] = {0x03,'\0'};

const char  CONST_FULL_SRING[] = "-------";


#define gstrnicmp	strncasecmp
#define gstricmp_gu	strcasecmp
#define _vsnprintf  vsnprintf

#define wlog printf


#define utils CSystemUtil

class CSystemUtil {
public:


    static std::string& string_format(std::string& str, const char * sFormat, ...)
    {

        va_list arglist;
        va_start(arglist, sFormat);
        int size = vsnprintf(NULL, 0, sFormat, arglist) + 2;

        va_end(arglist);
        char * s = new char[size];

        va_start(arglist, sFormat);
        size = vsnprintf(s, size, sFormat, arglist);

        va_end(arglist);

        str = s;

        delete [] s;
        s = NULL;
        return str;
    }
    
    
    static int splitString(
							   const std::string& src,
							   std::vector<std::string>& vs,
							   const std::string tok,
							   bool trim = false, std::string null_subst = "")
    {

        if( src.empty() || tok.empty() )
        {
            return 0;
        }
        vs.clear();                    
        int pre_index = 0, index = 0, len = 0;
        int isrcLen = (int)src.length();
        std::string strTemp(isrcLen,0);
        while( (index = src.find_first_of(tok, pre_index)) !=  -1 )
        {
            if( (len = index-pre_index) > 0 )
            {
                if ((len <= isrcLen - pre_index - 1) && pre_index >= 0 && len >= 0)
                {
                    strTemp = "";
                    strTemp = src.substr(pre_index, len);
                    vs.push_back(strTemp);
                }
                else
                {
                    vs.push_back(null_subst);
                }
            }
            else if(trim==false)
            {
                vs.push_back(null_subst);
            }
            pre_index = index+1;
            if (pre_index >= isrcLen)
            {
                break;
            }
        }
        std::string endstr(isrcLen,0);
        if (pre_index <= isrcLen && pre_index >= 0)
        {
            endstr = src.substr(pre_index);
        }
        
        if( trim==false )
        {
            vs.push_back( endstr.empty()? null_subst:endstr );
        }
        else if( !endstr.empty() )
        {
            vs.push_back(endstr);
        }
        else
        {
            vs.push_back(null_subst);
        }
        return (int)vs.size();
    }


	static std::string& trim_right (std::string & s, const std::string & t=" \t")
	{
		std::string::size_type i (s.find_last_not_of (t));
		if (i == std::string::npos)
			return s;
		else
			return s.erase (s.find_last_not_of (t) + 1) ;
	}
	
	static std::string& trim_left (std::string & s, const std::string & t=" \t")
	{
		return s.erase (0, s.find_first_not_of (t)) ;
	}
	
	static std::string& trim (std::string & s, const std::string & t = " \t")
	{
		return trim_left (trim_right (s, t), t) ;
	}

	static std::string& readFileAsString(const char *file, std::string &sReturn, int maxLine=8000)
	{
		sReturn = "";
		char buf[8001];
		int icnt = 0;

		FILE *fp = fopen(file,"r");
		memset(buf,0x00,8001);

		if ( fp == NULL )
		{
			return  sReturn;
		}

		while ( fgetline(fp,buf,8000) != EOF )
		{
			sReturn += buf;
			sReturn += "\n";

			icnt++;

			if ( icnt >= maxLine ) break;

		}
		if ( fp != NULL )
			fclose(fp);
		return sReturn;
	}


	static int  fgetline(FILE *fp, char *buffer, int maxlen)
	{
		int  i, j;
		char ch1;

		for(i = 0, j = 0; i < maxlen; j++)
		{
			if(fread(&ch1, sizeof(char), 1, fp) != 1)
			{
				if(feof(fp) != 0)
				{
					if(j == 0) return -1;               /* EOF */
					else break;
				}
				if(ferror(fp) != 0) return -2;        /* error */
				return -2;
			}
			else
			{
				if(ch1 == '\n' || ch1 == 0x00 )
					break; /* end of line */
				if(ch1 == '\f' || ch1 == 0x1A)        /* '\f': Form Feed */
				{
					buffer[i++] = ch1;
					break;
				}
				if(ch1 != '\r') buffer[i++] = ch1;    /* ignore CR */
			}
		}
		buffer[i] = '\0';
		return i;

	}
};

typedef std::vector<std::string> table_line;


class  CSelectHelp
{
public:

    int size()
    {
        return _count;
    }
    /* 浠嶤SelectHelp copy */


    //鐢熸垚璋冭瘯鏁版嵁锛屼粎浠呯敤浜庤皟璇�
    void GenDebugData()
    {
        reset();
        addField("name","杩愯惀鍟�");
        addField("age","骞寸邯");
        addField("use_id","缂栧彿");

        table_line line;
        line.push_back("涓浗鐢典俊");
        line.push_back("10");
        line.push_back("10000");
        addValue(line);
        line.clear();

        line.push_back("涓浗绉诲姩");
        line.push_back("12");
        line.push_back("10086");
        addValue(line);
        line.clear();

        line.push_back("涓浗鑱旈��");
        line.push_back("11");
        line.push_back("9090");
        addValue(line);
        line.clear();

    }
	void  unionHelp(CSelectHelp& s)
	{
		if ( s._fields.size() != this->_fields.size() )
		{
		
			 uint i;
			this->_fields.clear();
			this->_values.clear();
			this->_count =0;

			for(i=0; i<s._fields.size(); i++)
			{
				(*this).addField(s._fields[i]);
			}
		}
		int iCount = this->_count;
		uint i;
        for(i=0; i<s._values.size(); i++)
        {
            table_line line;
            for(uint j=0; j<s._fields.size(); j++)
            {
                line.push_back(s.valueString(i,j));
            }
            (*this).addValue(line);
            line.clear();
        }

        this->_count = s._count+iCount;
	}
    void copy(CSelectHelp& s)
    {
        uint i;
        this->_fields.clear();
        this->_values.clear();
        this->_count =0;

        for(i=0; i<s._fields.size(); i++)
        {
            (*this).addField(s._fields[i]);
        }

        for(i=0; i<s._values.size(); i++)
        {
            table_line line;
            for(uint j=0; j<s._fields.size(); j++)
            {
                line.push_back(s.valueString(i,j));
            }
            (*this).addValue(line);
            line.clear();
        }

        this->_count = s._count;
    }
    void copyStruct(CSelectHelp& s)
    {
        uint i;
        this->_fields.clear();
        this->_values.clear();
        this->_count =0;

        for(i=0; i<s._fields.size(); i++)
        {
            (*this).addField(s._fields[i]);
        }
        this->_count = 0;
    }

    const CSelectHelp& operator =(CSelectHelp& s)
    {
        for(uint i=0; i<s._fields.size(); i++)
        {
            (*this).addField(s._fields[i]);
        }

        return *this;
    }
    CSelectHelp()
    {
        reset();
        utils::string_format(_sSep , "%s",CONST_SEP_FIELD);
        utils::string_format(_sLineSep , "%s",CONST_SEP_LINE);
    }
    ~CSelectHelp()
    {
        reset();
    }


	int fromJsonString(const std::string& jsonStr)
	{
		return 0;
	}

    /* 閫氳繃瀛楃涓茶浆鍖栦负CSelectHelp,杩斿洖鍊间唬琛ㄥ鐞嗙殑鏉℃暟 */
    int fromString(const char* s,
                   const char* sSep,const char* sLineSep,bool bHaveHeader=true)
    {
        if ( strlen(sSep) <= 0 ||  strlen(sLineSep) <= 0 ) return -1;

        //璁剧疆鍒嗛殧绗�
        setSplit(sSep[0],sLineSep[0]);

        reset();

        std::string src = s;

        uint i=0;
        _count = 0;

        std::vector<std::string> vsAll;
        if ( utils::splitString(src.c_str(),vsAll,sLineSep) <= 0 )
        {
            return -1;
        }

        std::vector<std::string> vsLine;

        //澶勭悊澶�
        if ( utils::splitString(vsAll[0].c_str(),vsLine,sSep) <= 0 )
        {
            return -1;
        }

        //澧炲姞瀛楁
        for(i=0; i<vsLine.size(); i++)
        {
            if ( bHaveHeader )
            {

                addField(vsLine[i].c_str());
            }
            else
            {
                char buf[200]= {0};
                sprintf(buf,"f%03d",i);
                addField(buf);
            }
        }
        if ( vsAll.size() <= 1 )
        {
            /*鍙湁澶存病鏈夋暟鎹�*/
            return 0;
        }

		int ikk = 1;
		if ( bHaveHeader == false )
		{

			ikk = 0;

		}
        for(i=ikk; i<vsAll.size(); i++)
        {
            vsLine.clear();
            table_line line;
            if ( utils::splitString(vsAll[i].c_str(),vsLine,sSep) < (int)_fields.size() )
            {
                if ( vsLine.size() < _fields.size() )
                {
					//淇瀹屽叏绌哄瓧绗︿覆闂
					if ( vsAll[i].length() <= 0 && _fields.size() == 1 )
					{
						if ( _fields.size() > 1 )
						{
							line.push_back("");
							_values.push_back(line);
							_count++;
						}
						continue;
					}

                    continue;
                }
            }


            for(uint j=0; j<vsLine.size(); j++)
            {
                line.push_back(vsLine[j].c_str());
            }
            _values.push_back(line);
            _count++;
        }
        return _count;

    }
    int fromString(const char* src)
    {
        return fromString(src,_sSep.c_str(),_sLineSep.c_str(),true);
    }
	int fromVector(std::vector<std::vector<std::string> >& v)
	{
		_values = v;
		return true;
	}
    int fromStringNoField(const char* src,const char* sSep,const char* sLineSep)
    {
        return fromString(src,_sSep.c_str(),_sLineSep.c_str(),false);
    }

    bool existField(const char* src)
    {
        for(uint i=0; i<_fields.size(); i++)
        {
            if ( gstricmp_gu(_fields[i].c_str(),src) == 0 )
            {
                return true;
            }
        }

        return false;
    }
    std::string toString()
    {
        std::string sout;
        uint i= 0 ;
        for (i=0; i<_fields.size(); i++)
        {
            sout += _fields[i];
            sout += _sSep;
        }
        sout += _sLineSep;

        int iRow = 0;
        for (iRow=0; iRow<_count; iRow++)
        {

            for (i=0; i<_fields.size(); i++)
            {
                sout +=  valueString(iRow,i);
                sout += _sSep;
            }

            if ( iRow != _count-1 )
                sout += _sLineSep;

        }
        return sout;
    }


    /** 閫氳繃key,value鏌ユ壘鍙﹀鐨刱ey,value
    *  @param[in] 杈撳叆鐨刱ey
    *  @param[in] key瀵瑰簲鐨勫��
    *  @param[in] 鐩爣key
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    std::string search(const std::string& key,const std::string& value,const std::string& destKey)
    {
        int idx = getIndexByName(key);

        if ( idx >= (int)_fields.size() || idx<0 )
        {
            return "";
        }

        for(uint row=0; row<_values.size(); row++)
        {
            if (  gstricmp_gu(_values[row][idx].c_str(),value.c_str() ) == 0 )
            {
                int idx2 = getIndexByName(destKey);

                if (  idx2 >= 0 )
                    return _values[row][idx2];
                else
                    return "";

            }
        }

        return "";
    }
    /** 閫氳繃key,value鏌ユ壘鍙﹀鐨刱ey,value
    *  @param[in] 杈撳叆鐨刱ey
    *  @param[in] key瀵瑰簲鐨勫��
    *  @param[in] 鐩爣key
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    int search(const std::string& key,const std::string& value,std::vector<int>& vs)
    {
        vs.clear();
        int idx = getIndexByName(key);

        if ( idx >= (int)_fields.size() || idx<0 )
        {
            return -1;
        }

        for(uint row=0; row<_values.size(); row++)
        {
            if (  gstricmp_gu(_values[row][idx].c_str(),value.c_str() ) == 0 )
            {
                vs.push_back(row);
            }
        }

        return vs.size();
    }


    /** 鑾峰彇鎸囧畾琛岀殑鎸囧畾鍒楃殑鏁版嵁,浠ュ瓧绗︿覆鏂瑰紡杩斿洖
    *  @param[in] 琛屽彿
    *  @param[in] key
       @param[in] 鏄惁鍘绘帀宸﹀彸鐨勭┖鏍煎拰tab
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    std::string valueString(int row,std::string key,bool trim=false)
    {
        if ( row <0 ) return "";
        if ( row >= _count ) return "";

        int idx = getIndexByName(key);

        if ( idx >= (int)_fields.size() || idx<0 )
        {
            std::string sTmp;
            utils::string_format(sTmp,"瀛楁 \"%s\" 涓嶆纭�",key.c_str());
            return sTmp;
        }

        if ( trim )
        {
            std::string sTmp;
            sTmp = _values[row][idx];

            utils::trim(sTmp);

            return sTmp;

        }

        return _values[row][idx];
    }

    /** 鑾峰彇鎸囧畾琛岀殑鎸囧畾鍒楃殑鏁版嵁,浠ュ瓧绗︿覆鏂瑰紡杩斿洖
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    std::string valueString(int row,int col,bool trim=false)
    {
        if ( row <0 ) return "";
        if ( col >= (int)_fields.size() || col <0 )
        {
            return "瀛楁搴忓彿涓嶆纭�";
        }


        if ( trim )
        {
            std::string sTmp;
            sTmp = utils::trim(_values[row][col]);

            return sTmp;

        }

        return _values[row][col];
    }


    /** 鑾峰彇鎸囧畾琛岀殑鎸囧畾鍒楃殑鏁版嵁,浠ユ暣鏁版柟寮忚繑鍥�
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    int valueInt(int row,int col,int def=-999)
    {
        if (  row<0 ) return 0;

        if ( col >=(int) _fields.size() || col<0 ) return def;

        return  atol(_values[row][col].c_str());

    }

    /** 鑾峰彇鎸囧畾琛岀殑鎸囧畾鍒楃殑鏁版嵁,浠ユ暣鏁版柟寮忚繑鍥�
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    int valueInt(int row,std::string key,int def=-999)
    {
        if (  row<0 ) return 0;

        int idx = getIndexByName(key);
        if ( idx >= (int)_fields.size() || idx<0 ) return def;


        return  atol(_values[row][idx].c_str());
    }

    /** 鑾峰彇鎸囧畾琛岀殑鎸囧畾鍒楃殑鏁版嵁,浠ユ诞鐐规暟鏂瑰紡杩斿洖
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    double valuedouble(int row,int col,double def=-999.0)
    {
        if (  row<0 ) return 0;

        if ( col >= (int)_fields.size() || col<0 ) return def;

        return atof(_values[row][col].c_str());
    }

    /** 鑾峰彇鎸囧畾琛岀殑鎸囧畾鍒楃殑鏁版嵁,浠ユ诞鐐规暟鏂瑰紡杩斿洖
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @return 鎵惧埌鐨勫瓧绗︿覆
    */
    double valuedouble(int row,std::string key,double def=-999.0)
    {
        if (  row<0 ) return 0;

        int idx = getIndexByName(key);
        if ( idx >= (int)_fields.size() || idx<0 ) return def;
        return atof(_values[row][idx].c_str());
    }


    /** 閲嶆柊鍒濆鍖栧府鍔╃被	*/
    void reset()
    {
        _count = 0 ;
        _fields.clear();
        _values.clear();
        _alias.clear();
        _sSortField = "";
        _bUniqueSort = false;
    }


    //淇敼瀛楁鐨勫埆鍚�
    bool setFieldInfo(const std::string& field,const std::string& name,const std::string& alias="")
    {
        for(uint i=0; i<_fields.size(); i++)
        {
            if ( gstricmp_gu(_fields[i].c_str(),field.c_str()) == 0 )
            {
                _fields[i] = name;
                _alias[i] = alias;
                return true;
            }
        }
        return false;
    }
    //淇敼瀛楁鐨勫埆鍚�
    bool setFieldInfo(int idx,const string& name,const string& alias="")
    {

        if ( idx >= (int)_fields.size() || idx<0 ) return false;  //娌℃湁鎵惧埌瀛楁鍒欒繑鍥�


        _fields[idx] = name;
        _alias[idx] = alias;
        return true;
    }

    /** 鏄剧ず褰撳墠琛ㄦ煡璇㈣〃缁撴瀯鐨勪俊鎭�	*/
    void dumpSelectStruct()
    {

    }

    /*
    	灏嗘暟鎹寜鎸囧畾瀛楁鎺掑簭,蹇�熷害鏌ユ壘,闇�瑕佸厛浣跨敤CSortHelp杩涜鎺掑簭
    	閲囩敤宸叉帓搴忔柟寮�,閲囩敤浜屽垎鏌ユ壘锛屽彲浠ュぇ澶ф彁楂樻晥鐜�,浣嗗彧鑳借繑鍥炰竴鏉¤褰�
    */
    bool lookup(const char* field,const char* value,CSelectHelp& help)
    {


        help.reset();

        int idx = getIndexByName(field);
        if ( idx >= (int)_fields.size() || idx<0 ) return false;  //娌℃湁鎵惧埌瀛楁鍒欒繑鍥�


        help.copyStruct(*this);

        if ( !_bUniqueSort || _sSortField.length()<=0 )
        {
            //娌℃湁鎺掑簭杩囧垯杩斿洖
            //澶嶅埗缁撴瀯


            for(int i=0; i<_count; i++)
            {
                if ( strcmp( valueString(i,idx).c_str(),value) == 0 )
                {
                    //鎵惧埌鍒欏皢姝よ鍔犲叆help;

                    table_line line;
                    for(uint j=0; j<_fields.size(); j++)
                    {
                        line.push_back(valueString(i,j));
                    }
                    help.addValue(line);
                }
            }
            return true;
        }


        //std::binary_search(
        int   low=0,high=_count-1,mid;   //缃綋鍓嶆煡鎵惧尯闂翠笂銆佷笅鐣岀殑鍒濆��
        while( low <= high )
        {   //褰撳墠鏌ユ壘鍖洪棿R[low..high]闈炵┖
            mid=(low+high)/2;
            int ic = gstricmp_gu(valueString(mid,idx).c_str(),value );
            if(   ic   ==  0   )
            {
                table_line line;
                for(uint j=0; j<_fields.size(); j++)
                {
                    string stmp = valueString(mid,j);

                    line.push_back(stmp);
                    //wlog("%s\n",stmp.c_str());
                }
                help.addValue(line);

                //area = valueString(mid,2).c_str();
                //desc = valueString(mid,3).c_str();
                return true;
            }
            if(	ic > 0)
                high=mid-1;   //缁х画鍦≧[low..mid-1]涓煡鎵�
            else
                low=mid+1;   //缁х画鍦≧[mid+1..high]涓煡鎵�
        }
        return   false;   //褰搇ow>high鏃惰〃绀烘煡鎵惧尯闂翠负绌猴紝鏌ユ壘澶辫触
    }

    /** 鏄剧ず褰撳墠鍒楄〃涓殑鏁版嵁	*/
    void dump(const char* sep="\t")
    {
        for(uint k=0; k<_fields.size(); k++)
        {
            if ( _alias[k].length() <= 0 )
            {
                wlog("%s",_fields[k].c_str());
            }
            else
            {
                wlog("%s",_alias[k].c_str());
            }
            if ( k <= (_fields.size() -2) )
                wlog("%s",sep);
        }
        wlog("\n");

        for(uint i=0; i<_values.size(); i++)
        {
            for(uint j=0; j<_fields.size(); j++)
            {

                wlog("%s",valueString(i,_fields[j]).c_str());
                if ( j <= (_fields.size() -2) )
                    wlog("%s",sep);
            }
            wlog("\n");
        }
    }

    /** 澧炲姞table_line
    *  @param[in] table_line
    *  @return 濡傛槸ture浠ｈ〃鎴愬姛
    */
    bool addValue(table_line value)
    {
        if ( value.size() < _fields.size() ) return false;

        _values.push_back(value);
        _count++;

        return true;
    }


    /** 澧炲姞瀛楁鍚�
    *  @param[in] 瀛楁鍚�
    *  @return 娌℃湁杩斿洖鍊�
    */
    void addField(const std::string& value,const std::string& alias="")
    {
        _alias.push_back(alias);
        _fields.push_back(value);
    }

    /*鑾峰彇瀛楁鐨勫埆鍚�*/
    std::string getAlias(int col)
    {
        if ( col >= (int)_fields.size() || col<0 ) return "";

        if ( _alias[col].length() > 0  )
        {
            return _alias[col];
        }

        return  _fields[col];
    }

    std::string getAlias(const string& field)
    {

        for(uint k=0; k<_fields.size(); k++)
        {
            if ( gstricmp_gu(field.c_str(),_fields[k].c_str()) == 0 )
            {
                if ( _alias[k].length() <= 0 )
                {
                    return _fields[k];
                }
                return _alias[k];
            }
        }
        return "";
    }

    //鐢ㄤ簬鏇存柊help鍐呭

    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑鏁存暟鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setValueInt(int row,int col,int value)
    {
        if (  row<0 ) return false;

        if ( col >= (int)_fields.size() || col<0 ) return false;


        _values[row][col]= value;

        return false;
    }

    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑鏁存暟鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 瀛楁鍚�
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setValueInt(int row,std::string key,int value)
    {
        if (  row<0 ) return false;

        int idx = getIndexByName(key);
        if ( idx >= (int)_fields.size() || idx<0 ) return false;



        _values[row][idx] = value;

        return true;
    }



   bool setI(int row,int col,int value)
    {
        
		return setValueInt(row,col,value);
    }

    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑鏁存暟鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 瀛楁鍚�
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setI(int row,std::string key,int value)
    {
       return setValueInt(row,key,value);
    }


    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑娴偣鏁扮殑鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setValuedouble(int row,int col,double value)
    {
        if (  row<0 ) return false;

        if ( col >= (int)_fields.size() || col<0 ) return false;

        char buf[40]= {0};
        sprintf(buf,"%f",value);

        _values[row][col] = buf ;
        return false;
    }

    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑娴偣鏁扮殑鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 瀛楁鍚�
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setValuedouble(int row,std::string key,double value)
    {
        if (  row<0 ) return false;

        int idx = getIndexByName(key);
        if ( idx >= (int)_fields.size() || idx<0 ) return false;

        char buf[40]= {0};
        sprintf(buf,"%f",value);
        _values[row][idx] = buf ;

        return true;
    }
    bool setF(int row,int col,double value)
    {
		return setValuedouble(row,col,value);
    }

    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑娴偣鏁扮殑鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 瀛楁鍚�
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setF(int row,std::string key,double value)
    {
       
		return setValuedouble(row,key,value);
    }


    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑瀛楃涓茬殑鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 鍒楀彿
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setValueString(int row,int col,std::string value)
    {
        if (  row<0 ) return false;

        if ( col >= (int)_fields.size() || col<0 ) return false;


        _values[row][col] = value;

        return false;
    }

    /** 鍦ㄨ繑鍥炵殑缁撴灉闆嗕腑锛屼慨鏀圭粨鏋滈泦涓殑娴偣鏁扮殑鍊�
    *  @param[in] 琛屽彿
    *  @param[in] 瀛楁鍚�
    *  @param[in] 淇敼鐨勫��
    *  @return 鏄惁淇敼鎴愬姛
    */
    bool setValueString(int row,std::string key,std::string value)
    {
        if (  row<0 ) return false;

        int idx = getIndexByName(key);
        if ( idx >= (int)_fields.size() || idx<0 ) return false;



        _values[row][idx] = value;

        return true;
    }
	bool setS(int row,std::string key,std::string value)
	{
	
		return setValueString(row,key,value);
	}
	bool setS(int row,int col,std::string value)
	{
		return setValueString(row,col,value);
	}



    void setSplit(char sep,char lineSep)
    {
        CSystemUtil::string_format(_sSep,"%c",sep);
        CSystemUtil::string_format(_sLineSep,"%c",lineSep);

    }

    //灏咰SelectHelp杩涜鎺掑簭
    //鎺掑簭鍙互鎸塻tring,float,int涓夌鏂瑰紡杩涜鎺掑簭
    bool sort(const char* field,
              const char* type="string",
              bool bAsc=true)
    {
        //鍏堝皢鍘熸湁瀵硅薄杩涜Copy淇濆瓨鍘熸湁鍊�

        //鍒ゆ柇鏄惁瀛樺湪姝ゅ瓧娈靛悕
        if ( !existField(field)  ) return false;

        CSelectHelp nHelp;
        nHelp.copy(*this);

        reset();
        copyStruct(nHelp);



        int i=0;
        std::vector<std::string> vs;  //涓存椂瀵硅薄
        for(i=0; i<nHelp.size(); i++)
        {
            //澧炲姞鏃惰淇濊瘉鏄敮涓�鐨�

            vs.push_back(nHelp.valueString(i,field));
        }


        std::vector<std::string>::iterator it =  std::unique(vs.begin(), vs.end());
        vs.erase(it, vs.end());


        //vector<string>::iterator new_end = std::unique(vs.begin(), vs.end());
        wlog("%d\n",vs.size());

        help_sort(vs,type,bAsc);


        //copy(vs.begin(),vs.end(),ostream_iterator<std::string>(cout,"\n"));


        //if ( nHelp.size() !=  vs.size() )
        //{
        //	reset();
        //	copy(nHelp);
        //	wlog("Error:size not equal\n");
        //	return false;
        //}

        _bUniqueSort = true;

        for(i=0; i<(int)vs.size(); i++)
        {

            std::vector<int> vint;

            if ( nHelp.search(field,vs[i],vint) <= 0 )
            {
                wlog("鍑洪敊浜嗭紝娌℃湁鎵惧埌,鍙兘鏄暟鎹被鍨嬩笉涓�鑷碶n");
                reset();
                copy(nHelp);
                _bUniqueSort = false;
                return false;
            }

            if ( vint.size() >1 )
            {
                _bUniqueSort = false;
            }

            //copy(vint.begin(),vint.end(),ostream_iterator<int>(cout,"\n"));

            for(uint j=0; j<vint.size(); j++)
            {
                if ( j%100 == 0 )
                {
                    //wlog("%d\n",j);
                }
                table_line iline;
                for(uint k=0; k<nHelp._fields.size(); k++)
                {

                    iline.push_back(nHelp.valueString(vint[j],k));
                }

                addValue(iline);
            }


        }

        _sSortField = field;

        return (_count == nHelp._count);
    }


    //for lua only

    bool toFile(string file)
    {
        FILE* fp = fopen(file.c_str(),"w");

        if ( fp == NULL ) return false;


        string s = toString();

        fwrite(s.c_str(),sizeof(char),s.length(),fp);

        fclose(fp);

        return true;

    }
    bool fromFile(string file)
    {
        string out;
        utils::readFileAsString((char*)file.c_str(),out,80000);

        fromString(out.c_str());

        return true;
    }

    std::string	vsi(int row,int col)
    {


        return valueString(row,col);
    }
    std::string vs(int row,string  field)
    {
        return valueString(row,field);
    }

    long	vii(int row,int col)
    {

        return valueInt(row,col);
    }
    long vi(int row,string field)
    {
        return valueInt(row,field);
    }

    double	vfi(int row,int col)
    {


        return valuedouble(row,col);
    }
    double vf(int row,string field)
    {
        return valuedouble(row,field);
    }

    int	cols()
    {
        return (int)_fields.size();
    }
    int	rows()
    {
        return _count;
    }
    std::string getColName(int col)
    {
        if ( col >= cols() ) return "";

        return _fields[col];
    }


public:

    bool			_bUniqueSort;
    std::string		_sSortField;
    std::string		_sSep;
    std::string		_sLineSep;
    /** 杩斿洖鐨勮褰曟潯鏁� */
    int _count;

    /** 瀛樺偍鏁版嵁搴撹繑鍥炵殑鍚勮淇℃伅 */
    std::vector<table_line> _values;

    /** 杩斿洖鐨勫瓧娈靛悕鏁扮粍 */
    std::vector<std::string>   _fields;

    /** 杩斿洖鐨勫瓧娈靛悕鏁扮粍 */
    std::vector<std::string>   _alias;  //瀛楁鐨勫埆鍚�

    /** 閫氳繃鍚嶇О鏌ヨ鐩稿簲鐨勭储寮曞彿 */
    int getIndexByName(std::string key)
    {
        for(uint i=0; i<_fields.size(); i++)
        {

            if ( gstricmp_gu(key.c_str(), _fields[i].c_str()) == 0 )
                return i;
        }
        return -1;
    }
private:


    static int comp_int_asc(const int& a,const int& b)
    {
        return a < b;
    }
    static int comp_int_dsc(const int& a,const int& b)
    {
        return a > b;
    }
    static int comp_float_asc(const double& a,const double& b)
    {
        return a < b;
    }
    static int comp_float_dsc(const double& a,const double& b)
    {
        return a > b;
    }

    static int comp_string_asc(const string& a,const string& b)
    {
        return ( strcmp(a.c_str(),b.c_str()) < 0 );
    }
    static int comp_string_dsc(const string& a,const string& b)
    {
        return ( strcmp(a.c_str(),b.c_str()) > 0 );
    }

    static void help_sort(std::vector<std::string> &vs,
                          const char* type="string",bool bAsc=true)
    {

        //鎸夊瓧绗︿覆鎺掑簭
        if ( gstricmp_gu(type,"string") ==  0)
        {

            if ( bAsc )
                std::sort(vs.begin(), vs.end(),comp_string_asc);
            else
                std::sort(vs.begin(), vs.end(),comp_string_dsc);

            return;
        }

        if ( gstricmp_gu(type,"int") ==  0)
        {
            //濡傛灉鏄暣鏁帮紝鍒欓渶瑕佸缓涓存椂瀵硅薄鍐嶆帓搴�
            std::vector<int> vsTmp;

            for(uint i=0; i<vs.size(); i++)
            {

                vsTmp.push_back( atoi(vs[i].c_str()) );
            }

            if ( bAsc )
                std::sort(vsTmp.begin(), vsTmp.end(),comp_int_asc);
            else
                std::sort(vsTmp.begin(), vsTmp.end(),comp_int_dsc);

            //灏嗘暟鎹啀copy鍥瀡s
            vs.clear();
            for(uint i=0; i<vsTmp.size(); i++)
            {
                string stmp;
                utils::string_format(stmp,"%d",vsTmp[i]);
                vs.push_back( stmp);
            }
            return;
        }
        if ( gstricmp_gu(type,"float") ==  0)
        {
            //濡傛灉鏄暣鏁帮紝鍒欓渶瑕佸缓涓存椂瀵硅薄鍐嶆帓搴�
            std::vector<double> vsTmp;

            for(uint i=0; i<vs.size(); i++)
            {
                vsTmp.push_back( atof(vs[i].c_str()) );
            }
            if ( bAsc )
            {
                std::sort(vsTmp.begin(), vsTmp.end(),comp_float_asc);
            }
            else
            {
                std::sort(vsTmp.begin(), vsTmp.end(),comp_float_dsc);
            }

            //灏嗘暟鎹啀copy鍥瀡s
            vs.clear();
            for(uint i=0; i<vsTmp.size(); i++)
            {
                string stmp;
                utils::string_format(stmp,"%.8f",vsTmp[i]);
                utils::trim_right(stmp,"0");
                utils::trim_right(stmp,".");

                vs.push_back( stmp);
            }
            return;
        }
    }

};

