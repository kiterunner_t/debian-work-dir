#pragma once

#include <string>

#include "Request.hh"


typedef enum {
    kResponseEmpty = 0,
    kResponseOk = 200
} ResponseCode;


typedef enum {
    kSuccess = 0
} ErrorCode;


class Response {
public:
    Response();
    ~Response();


    void clear();
    bool checkStatus();


    void setRequest(Request* request) { _request = request; }

    ResponseCode getCode() { return _statusCode; }
    void setCode(ResponseCode code) { _statusCode = code; }

    std::string& getErrorMessage() { return _errorMessage; }

    std::string& getBody() { return _body; }
    void setBody(const std::string& body) { _body = body; }
    

private:
    bool _parseResult();


private:
    Request* _request;

    ResponseCode _statusCode;
    std::string _errorMessage;

    std::string _body;
};

