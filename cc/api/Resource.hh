#pragma once

#include <string>

#include "eutils_arm_v2.h"
#include "HttpTransport.hh"
#include "Request.hh"
#include "Response.hh"


class Resource {
public:
    Resource();
    ~Resource();


protected:
    void setModuleName(std::string& moduleName);
    void setResourceName(std::string& resourceName);
    
    bool get(CSelectHelp& help);
    bool post();
    bool put();
    bool del();

    bool batch();

    
private:
    bool _doModify();


protected:
    GetRequest _getRequest;
    ModifyRequest _modifyRequest;


private:
    HttpTransport _transport;
    Response _response;
};

