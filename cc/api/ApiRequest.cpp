#include <cstdio>
#include <cstdarg>
#include <string>

#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "Request.hh"

using namespace rapidjson;
using namespace std;


const string Request::kBaseUrl = "http://192.168.239.129/mes";


Request::Request()
    : _type(kGet),
      _isResourceId(false),
      _query(new Document(rapidjson::kObjectType)),
      _body(new Document(rapidjson::kObjectType))
{
}


Request::~Request()
{
    if (_query) {
        delete _query;
        _query = NULL;
    }

    if (_body) {
        delete _body;
        _body = NULL;
    }
}


const std::string Request::getUrl()
{
    string path = "/";
    string resource = _resourceName.empty() ? "" : _resourceName + "/";
    string module = _moduleName.empty() ? "" : _moduleName + "/";

    char t[32];
    sprintf(t, "%d", _resourceId);
    string id = _isResourceId ? t : "/";
    path += resource + module + id;
    path = path.substr(0, path.length() - 2);
    
    string queryStr;
    for (Value::ConstMemberIterator iter = _query->MemberBegin();
         iter != _query->MemberEnd();
         ++iter)
    {
        if (queryStr.empty() == false) {
            queryStr += "&";
        }

        queryStr += _urlEncode(iter->name.GetString());
        queryStr += "=";
        if (iter->value.IsString()) {
            queryStr += _urlEncode(iter->value.GetString());
        } else if (iter->value.IsNumber()) {
            char t[32];
            sprintf(t, "%d", iter->value.GetInt());
            queryStr += t;
        } else if (iter->value.IsBool()) {
            queryStr += iter->value.GetBool() ? "true" : "false";
        } else {
            assert(0);
        }
    }
    
    printf("%s\n", path.c_str());
    return queryStr.empty() ? kBaseUrl + path : kBaseUrl + path + "?" + queryStr;
}


// https://en.wikipedia.org/wiki/Percent-encoding
std::string Request::_urlEncode(const string& url)
{
    static const char* const hex = "0123456789ABCDEF";

    size_t len = url.length();
    string encodeUrl;
    for (size_t i = 0; i < len; ++i) {
        unsigned char c = static_cast<unsigned char>(url[i]);
        if (isalnum(c) || c=='-' || c=='_' || c=='.' || c=='~') {
            encodeUrl += c;

        } else {
            encodeUrl += '%';
            encodeUrl += hex[c>>4];
            encodeUrl += hex[c&0xF];
        }
    }
    
    return encodeUrl;
}


const std::string Request::getBody()
{
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
    _body->Accept(writer);
    
    return buf.GetString();
}


void Request::addToken(const char* token)
{
    rapidjson::Value value(token, _query->GetAllocator());
    _query->AddMember("token", value, _query->GetAllocator());
}


void Request::clear()
{
    _isResourceId = false;
    
    if (_query) {
        delete _query;
        _query = new rapidjson::Document(rapidjson::kObjectType);
    }

    if (_body) {
        delete _body;
        _body = new rapidjson::Document(rapidjson::kObjectType);
    }
}


void Request::addQuery(const char* key, int value)
{
    rapidjson::Value k(key, _query->GetAllocator());
    _query->AddMember(k, value, _query->GetAllocator());
}


void Request::addQuery(const char* key, const char* value)
{
    rapidjson::Value k(key, _query->GetAllocator());
    rapidjson::Value v(value, _query->GetAllocator());
    _query->AddMember(k, v, _query->GetAllocator());
}


void Request::addQuery(const char* key, bool value)
{
    rapidjson::Value k(key, _query->GetAllocator());
    _query->AddMember(k, value, _query->GetAllocator());
}



void GetRequest::setPage(int pageNum, int pageSize)
{
    if (pageNum <= 0 || pageSize <= 0) {
        assert(0);
        return ;
    }

    _body->AddMember("page", pageNum, _query->GetAllocator());
    _body->AddMember("pageSize", pageSize, _query->GetAllocator());
}


void GetRequest::setField(int argCount, ...)
{
    va_list ap;
    va_start(ap, argCount);
    _setBodyField("field", argCount, ap);
    va_end(ap);
}


void GetRequest::setOrder(int argCount, ...)
{
    va_list ap;
    va_start(ap, argCount);
    _setBodyField("order", argCount, ap);
    va_end(ap);
}


void GetRequest::_setBodyField(const char* key, int argCount, va_list ap)
{
    rapidjson::Value array(rapidjson::kArrayType);
    Document::AllocatorType& allocator = _body->GetAllocator();

    for (int i = 0; i < argCount; ++i) {
        char* p = va_arg(ap, char*);
        array.PushBack(Value(p, allocator).Move(), allocator);
    }

    rapidjson::Value k(key, allocator);
    _body->AddMember(k, array, allocator);
}


void GetRequest::addBodyQuery(const char* key, int value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("query");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("query", v, _body->GetAllocator());
    }

    rapidjson::Value k(key, _query->GetAllocator());
	rapidjson::Document& body = *_body;
	body["query"].AddMember(k, value, _body->GetAllocator());
}


void GetRequest::addBodyQuery(const char* key, const char* value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("query");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("query", v, _body->GetAllocator());
    }
    
    rapidjson::Value k(key, _query->GetAllocator());
    rapidjson::Value v(value, _query->GetAllocator());
    rapidjson::Document& body = *_body;
	body["query"].AddMember(k, v, _query->GetAllocator());
}


void GetRequest::addBodyQuery(const char* key, bool value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("query");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("query", v, _body->GetAllocator());
    }
    
    rapidjson::Value k(key, _query->GetAllocator());
    rapidjson::Document& body = *_body;
	body["query"].AddMember(k, value, _query->GetAllocator());
}



void ModifyRequest::addData(const char* key, bool value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("data");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("data", v, _body->GetAllocator());
    }

    rapidjson::Value k(key, _query->GetAllocator());
	rapidjson::Document& body = *_body;
	body["data"].AddMember(k, value, _body->GetAllocator());
}


void ModifyRequest::addData(const char* key, int value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("data");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("data", v, _body->GetAllocator());
    }

    rapidjson::Value k(key, _query->GetAllocator());
	rapidjson::Document& body = *_body;
	body["data"].AddMember(k, value, _body->GetAllocator());
}


void ModifyRequest::addData(const char* key, const char* value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("data");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("data", v, _body->GetAllocator());
    }

    rapidjson::Value k(key, _query->GetAllocator());
    rapidjson::Value v(value, _query->GetAllocator());
	rapidjson::Document& body = *_body;
	body["data"].AddMember(k, v, _body->GetAllocator());
}

