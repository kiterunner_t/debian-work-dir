#include <string.h>

#include <rapidjson/document.h>

#include "Log.hh"
#include "Request.hh"
#include "Response.hh"


Response::Response()
    : _statusCode(kResponseEmpty),
      _errorMessage(""),
      _body(""), 
      _request(NULL)
{
}


Response::~Response() {}


void Response::clear()
{
    _statusCode = kResponseEmpty;
    _body = "";
    _errorMessage = "";
    _request = NULL; 
}


bool Response::checkStatus()
{
    if (_statusCode != kResponseOk) {
        return false;
    }

    return _parseResult();
}


bool Response::_parseResult()
{
    if (_request->getType() == kGet) {
        return true;
    }

    const std::string& jsonStr = _body;
    rapidjson::Document root;
    rapidjson::ParseResult result = root.Parse(jsonStr.c_str());
    if (!result) {
        log_warn("invalid json: %s", jsonStr.c_str());
        return false;
    }
    
    rapidjson::Value::ConstMemberIterator iter = root.FindMember("errCode");
    if (iter == root.MemberEnd()) {
        log_warn("not found result field: %s", jsonStr.c_str());
        return false;
    }

    // @todo ���ô����ַ���
    int errCode = iter->value.GetInt();
    if (errCode != kSuccess) {
        log_warn("exec batch error: ");
        return false;

    } else {
        return true;
    }
}

