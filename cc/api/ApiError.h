#pragma once

#include "rtx_control_interface.h"

typedef enum {
    API_SUCCESS = 0,
    API_ERROR,
    API_ENQUEUE,
    API_ERROR_OFFLINE,
    API_ERROR_SERVER_TIMEOUT,
    API_ERROR_FAILED,

    API_ERROR_MAX
} ApiErrorCode;


class CApiError {
public:
    static const string& getErrorMessage(int code);


private:
    static const string astrApiErrorMessage[];
};

