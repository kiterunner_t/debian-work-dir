#include <string>

#include "Resource.hh"
#include "Select.hh"
#include "Log.hh"

using namespace std;


class TestResource: public Resource {
public:
    TestResource() {
        setModuleName(kModuleName);
        setResourceName(kResourceName);
    }


    int get1(CSelectHelp& help) {
        _getRequest.addQuery("type", 1);
        _getRequest.addQuery("name", "中文");
        _getRequest.addQuery("bool", true);

        _getRequest.setField(2, "test", "测试");
        _getRequest.setOrder(3, "hello", "world", "test");

        _getRequest.addBodyQuery("bodyquery", 1);
        _getRequest.addBodyQuery("test", "中文");
        return get(help);
    }


    bool post1() {
        _modifyRequest.addData("type", 1);
        _modifyRequest.addData("test", false);
        _modifyRequest.addData("中文", "测试");

        return post();
    }


private:
    static string kModuleName;
    static string kResourceName;
};


string TestResource::kModuleName = "toolsmanage";
string TestResource::kResourceName = "tools";


int main()
{
    log_init();

    CSelectHelp help;
    TestResource r;

    r.get1(help);

    printf("\n\n");
    r.post1();
}

