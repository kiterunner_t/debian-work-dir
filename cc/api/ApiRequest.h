#pragma once

#include <cstdarg>
#include <string>

#include <rapidjson/document.h>


typedef enum {
    kGet = 0,
    kPost,
    kPut,
    kDel,
    kBatch
} RequestType;


class Request {
public:
    Request();
    ~Request();


	void clear();
	
    const std::string getUrl();
    const std::string getBody();

    // template <typename T> void addQuery(const char* key, T value);
    void addQuery(const char* key, int value);
    void addQuery(const char* key, const char* value);
    void addQuery(const char* key, bool value);
    void addToken(const char* token);


    RequestType getType() { return _type; }
    void setType(RequestType typ) { _type = typ; }

    int getResourceId() { return _isResourceId==true ? _resourceId : -1; }
    void setResourceId(int id) { _resourceId = id; _isResourceId = true; }

    const std::string& getModuleName() { return _moduleName; }
    void setModuleName(std::string& name) { _moduleName = name; }

    const std::string& getResourceName() { return _resourceName; }
    void setResourceName(std::string& name) { _resourceName = name; }


private:
    Request(const Request& r);
    Request& operator =(const Request& r);
    
    std::string _urlEncode(const std::string& url);


protected:
	rapidjson::Document* _query;
    rapidjson::Document* _body;
    

private:
    static const std::string kBaseUrl;

    RequestType _type;
    std::string _moduleName;
    std::string _resourceName;

    int _resourceId;
    bool _isResourceId;
};


/*
http://192.168.239.129/mes/lpa/roleoccur/1?roleId=1&page=1&pageSize=10&token=krt

{
    "field": ["roleId", "occurId", "time"],
    "order": ["roleId", "occurId"],
    "query": {
        "roleId": 1,
        "name": ["~", "fadf"],
        "time": ["range", "2016-10-01", "2016-10-01"],
        "int": ["range", 5, 10]
    }
}
*/
class GetRequest: public Request {
public:
    GetRequest() { setType(kGet); }
    ~GetRequest() {}

    void setPage(int pageNum, int pageSize);
    
    void setField(int argCount, ...);
    void setOrder(int argCount, ...);
    
    void addBodyQuery(const char* key, int value);
    void addBodyQuery(const char* key, const char* value);
    void addBodyQuery(const char* key, bool value);


private:
	 void _setBodyField(const char* key, int argCount, va_list ap);
};


/*
{
	"operand": "lpaRole",
	"operator": "update",
	"param": "roleId=5",
	"data": {
		"roleId": 10,
		"roleName": "2016-09-29 00:00:00"
	}
}

如果是batch的话，则会有多个，当前不支持
[
    {},
    {}
]
*/
class ModifyRequest: public Request {
public:
    ModifyRequest() {}
    ~ModifyRequest() {}

    void addData(const char* key, bool value);
    void addData(const char* key, int value);
    void addData(const char* key, const char* value);

    template <typename T> void addParam(const char* key, T value);
    
    // void addChild(const ModifyRequest& child);
    // void addSibling(const ModifyRequest& sibling);
    
    
private:
    // vector<ModifyRequest> _children;
    // vector<ModifyRequest> _siblings;
};


template <typename T>
void ModifyRequest::addParam(const char* key, T value)
{
    rapidjson::Value::ConstMemberIterator iter = _body->FindMember("param");
    if (iter == _body->MemberEnd()) {
        rapidjson::Value v(rapidjson::kObjectType);
        _body->AddMember("param", v, _body->GetAllocator());
    }

    rapidjson::Value k(key, _query->GetAllocator());
	rapidjson::Document& body = *_body;
	body["param"].AddMember(k, value, _body->GetAllocator());
}

