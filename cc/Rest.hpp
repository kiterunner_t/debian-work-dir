// http://www.cnblogs.com/li0803/archive/2008/11/03/1324746.html
// https://curl.haxx.se/libcurl/c/libcurl-tutorial.html
// http://www.cnblogs.com/hanframe/p/4241983.html
// http://www.cnblogs.com/moodlxs/archive/2012/10/15/2724318.html

// http://blog.jobbole.com/105079/
// http://blog.csdn.net/haoel/article/details/24058
// http://coolshell.cn/articles/10478.html

#ifndef REST_H_
#define REST_H_

#include <cstddef>


typedef enum {
    REST_SELECT,
    REST_POST
} RestType;


class Rest {
public:
    static bool globalInit();
    static void setDebug(bool debug);


private:
    static size_t _write(void* buf, size_t size, size_t nmemb, void* p);


private:
    static bool _debug;


public:
    Rest();
    ~Rest();

    bool post();

    std::string& getUrl() { return _url; }
    void setUrl(std::string& url) { _url = url; }
    void setUrl(const char* url) { _url = url; }

    std::string& getBody() { return _body; }
    void setBody(std::string& body) { _body = body; }
    void setBody(const char* body) { _body = body; }

    std::string& getResponse() { return _response; }


private:
    std::string _url;
    std::string _body;
    std::string _response;
};


#endif

