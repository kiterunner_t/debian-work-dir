#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;


void format(string& dst, const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    
    char a[1024];
    int size = vsnprintf(a, sizeof(a), fmt, args);
    va_end(args);
    
    if (size >=0 && size < static_cast<int>(sizeof(a))) {
        dst.append(a, size);
        return ;
    }
    
    printf("case: too large\n");
    return ;
}


int main()
{
    ostringstream ss;
    
    int m = 100;
    ss << m << " " << "ok";
    string s = ss.str();
    m = 10;

    cout << s << endl;
    
    char a[m];
    printf("%d\n", sizeof(a));
    
    
    string dst;
    format(dst, "test %d\n", 8);
    cout << dst;
    
    return 0;
}

