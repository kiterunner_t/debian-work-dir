#include <stdlib.h>
#include <string.h>

#include "String.hpp"


String::String()
{
    _size = 0;
    _capacity = 0;
    _s = NULL;
}


String::String(int capacity)
{
    _size = 0;
    _capacity = capacity;
    _s = new char[capacity];
}


String::String(char* s)
{
    _size = strlen(s);
    _capacity = _size + 1;
    _s = new char[_capacity];
    memcpy(_s, s, _capacity);
}


String::~String()
{
    _size = 0;
    _capacity = 0;
    delete _s;
}

