#include <cstdio>
#include <cstdlib>
#include <string>

#include <curl/curl.h>

#include "Rest.hpp"

#define wlog printf


using namespace std;


bool Rest::_debug = true;


bool Rest::globalInit()
{
    curl_global_init(CURL_GLOBAL_ALL);
}


void Rest::setDebug(bool debug)
{
    Rest::_debug = debug;
}


Rest::Rest()
{
}


Rest::~Rest()
{
}


static size_t _write(void* buf, size_t size, size_t nmemb, void* p)
{
    string* s = static_cast<string*>(p);
    if (s == NULL || buf == NULL) {
        return -1;
    }
    
    char* data = static_cast<char*>(buf);
    size_t totalSize = size * nmemb;
    s->append(data, totalSize);
    return totalSize;
}


bool Rest::post()
{
    _response = "";
    CURL* curl = curl_easy_init();
    if (curl == NULL) {
        wlog("curl init error");
        return false;
    }
    
    CURLcode ret;
    curl_slist* httpHeader = NULL;
    httpHeader = curl_slist_append(httpHeader, "Accept: application/json");
    httpHeader = curl_slist_append(httpHeader, "Content-Type: application/json");
    httpHeader = curl_slist_append(httpHeader, "charsets: utf-8");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, httpHeader);

    if (Rest::_debug) {
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    }

    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DEL");
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 1);

    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*) &_response);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ::_write);
    
    bool retCode = true;
    ret = curl_easy_setopt(curl, CURLOPT_URL, _url.c_str());
    if (ret != CURLE_OK) {
        wlog("failed set transfer url: %s, error: %d", _url.c_str(), ret);
        retCode = false;
        goto ERROR;
    }
    
    ret = curl_easy_setopt(curl, CURLOPT_POSTFIELDS, _body.c_str());
    if (ret != CURLE_OK) {
        wlog("failed set transfer body: %s, error: %d", _body.c_str(), ret);
        retCode = false;
        goto ERROR;
    }

    ret = curl_easy_perform(curl);
    if (ret != CURLE_OK) {
        wlog("error");
        retCode = false;
        goto ERROR;
    }

ERROR:
    curl_slist_free_all(httpHeader);
    curl_easy_cleanup(curl);
    return retCode;
}


int main(int argc, char** argv)
{
    Rest::globalInit();
    
    Rest* rest = new Rest();
    
    rest->setUrl("http://192.168.239.129/mes/lpa/roleoccur");
    rest->setBody("{\"key\":\"value\"}");
    
    string response;
    rest->post();
    
    printf("response:\n%s\n", rest->getResponse().c_str());

    delete rest;
}

