#ifndef STRING_HPP_
#define STRING_HPP_


class String {
public:
    String();
    String(int capacity);
    String(char* s);
    String(String& s);
    
    ~String();


private:
    int   _size;
    int   _capacity;
    char* _s;
};


#endif

