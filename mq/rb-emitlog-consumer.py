#! /usr/bin/env python

import pika


credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.136",
                                       credentials=credential)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

'''
channel.exchange_declare(exchange="logs_durable", type="fanout", durable=True)

result = channel.queue_declare(exclusive=True, durable=True)
queue_name = result.method.queue

channel.queue_bind(exchange="logs_durable", queue=queue_name)
'''

channel.queue_declare(queue='task_queue', durable=True) 

print " [*] waiting for logs. To exit pres Ctrl+C"


def callback(ch, method, properties, body):
    print " [x] %r" % (body)


channel.basic_consume(callback, queue='task_queue', no_ack=True)
channel.start_consuming()

