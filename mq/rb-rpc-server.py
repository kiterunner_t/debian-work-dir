#! /usr/bin/env python

import pika


credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.132",
                                       credentials=credential)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.queue_declare(queue="rpc_queue")


def fib(n):
    if n == 0:
        return 0;
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def on_request(channel, method, props, body):
    n = int(body)

    print " [.] fib(%s)" % (n)

    response = fib(n)

    basic_props = pika.BasicProperties(correlation_id=props.correlation_id)
    channel.basic_publish(exchange="", routing_key=props.reply_to,
                          properties=basic_props,
                          body=str(response))

    channel.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue="rpc_queue")

print " [x] Awaiting RPC requests"
channel.start_consuming()

