#! /usr/bin/env python

import pika
import sys


credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.132",
                                       credentials=credential)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.exchange_declare(exchange="topic_logs", type="topic")

routing_key = sys.argv[1] if len(sys.argv) > 1 else "anonymous.info"
message = " ".join(sys.argv[2:]) or "hello world"

channel.basic_publish(exchange="topic_logs", routing_key=routing_key, body=message)
print " [x] sent %r:%r" % (routing_key, message)

connection.close()

