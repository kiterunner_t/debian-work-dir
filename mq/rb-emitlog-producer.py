#! /usr/bin/env python

import pika
import sys
import time


is_durable = True
if is_durable:
    durable = True
    delivery_mode = 2
else:
    durable = False
    delivery_mode = 1

    
credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.136", credentials=credential)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

#channel.exchange_declare(exchange="logs_durable", type="fanout", durable=True)
channel.queue_declare(queue='task_queue', durable=durable) 

bytes_num = int(sys.argv[1])
count = int(sys.argv[2])
message = " ".join(sys.argv[3:]) or "x" * bytes_num

print "bytes:", bytes_num
print "count:", count

start = time.time()
for i in xrange(0, count):
    channel.basic_publish(exchange="", routing_key="task_queue", body=message,
                          properties=pika.BasicProperties(delivery_mode=delivery_mode))

end = time.time()
diff = end - start
if diff == 0:
    diff = 0.001
print "time count: ", count / diff

# print " [x] sent %r" % (message)
connection.close()

