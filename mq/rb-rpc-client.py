#! /usr/bin/env python

import pika
import uuid


class FibRpcClient(object):
    def __init__(self):
        credential = pika.PlainCredentials("test", "test")
        parameters = pika.ConnectionParameters(host="192.168.239.132",
                                               credentials=credential)
        
        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)


    def on_response(self, channel, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body


    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        basic_props = pika.BasicProperties(reply_to=self.callback_queue,
                                           correlation_id=self.corr_id)
        self.channel.basic_publish(exchange="",
                                   routing_key="rpc_queue",
                                   properties=basic_props,
                                   body=str(n))

        while self.response is None:
            self.connection.process_data_events()

        return int(self.response)


fib_rpc = FibRpcClient()

print " [x] Requesting fib(30)"
response = fib_rpc.call(30)
print " [.] Got %r" % (response)

