#! /usr/bin/env python

import os
import time
import sys

import pika


if len(sys.argv) != 2:
    print "help"
    os._exit(1)

try:
    is_durable = bool(int(sys.argv[1]))
except:
    print "help"
    os._exit(1)
    
print sys.argv
print "durable:", is_durable

if is_durable == True:
    durable = True
    delivery_mode = 2
    queue_name = "task_queue_durable"
else:
    queue_name = "non-task_queue_durable"
    durable = False
    delivery_mode = 1

credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.136",
                                       credentials=credential)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

'''
channel.exchange_declare(exchange="logs_durable", type="fanout", durable=True)

result = channel.queue_declare(exclusive=True, durable=True)
queue_name = result.method.queue

channel.queue_bind(exchange="logs_durable", queue=queue_name)
'''

channel.queue_declare(queue=queue_name, durable=durable) 

print " [*] waiting for logs. To exit pres Ctrl+C"


start = time.time()
count = 0
def callback(ch, method, properties, body):
    global count
    count = count + 1
    print " [x] %r" % (body)
    if body == "shutdown":
        global start
        diff = time.time() - start
        if diff == 0:
            diff = 0.001

        print "time: ", diff
        print "msg count: ", count
        print "count per sec: ", count / diff
        os._exit(0)


channel.basic_consume(callback, queue=queue_name, no_ack=True)
channel.start_consuming()

