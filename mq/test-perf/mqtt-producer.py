#! /usr/bin/env python
# coding: utf-8

import os
import sys
import time

import paho.mqtt.client as mqtt


if len(sys.argv) != 4:
    print "help"
    os._exit(1)

print sys.argv

try:
    bytes_num = int(sys.argv[1])
    count = int(sys.argv[2])
    is_durable = bool(int(sys.argv[3]))
except:
    print "help"
    os._exit(1)

msg_body = "x" * bytes_num

print "bytes:", bytes_num
print "count:", count
print "durable:", is_durable

if is_durable == True:
    clean_session = False
    queue_name = "/mqtt/durable"
    client_id = "mqtt-perf-producer-durable"
else:
    queue_name = "/mqtt/nondurable"
    clean_session = True
    client_id = "mqtt-perf-producer-nondurable"


ip = "192.168.239.136"
port = 1883
user = "test"
passwd = "test"


def on_connect(mqttc, obj, flags, rc):
    print("rc: "+str(rc))

def on_message(mqttc, obj, msg):
    print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

def on_publish(mqttc, obj, mid):
    print str(obj)
    print str(mid)
    

'''
def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)
'''

client = mqtt.Client(client_id=client_id, clean_session=clean_session)
#client.on_message = on_message
client.on_connect = on_connect
client.on_publish = on_publish
#client.on_subscribe = on_subscribe
#client.on_log = on_log

client.username_pw_set(user, password=passwd)
client.connect(ip, port, 60)

start = time.time()
for i in xrange(0, count):
    infot = client.publish(queue_name, msg_body, qos=1)

infot = client.publish(queue_name, "shutdown", qos=1)
infot.wait_for_publish()
client.disconnect()

end = time.time()
diff = end - start
if diff == 0:
    diff = 0.001

print "time: ", diff
print "msg count: ", count + 1
print "count per sec: ", count / diff
