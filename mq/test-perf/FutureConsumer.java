package example;

import org.fusesource.hawtbuf.*;
import org.fusesource.mqtt.client.*;

import java.util.List;
import java.util.ArrayList;


class FutureConsumer {
    public static void main(String[] args) throws Exception {
        String user = env("ACTIVEMQ_USER", "admin");
        String password = env("ACTIVEMQ_PASSWORD", "password");
        String host = env("ACTIVEMQ_HOST", "localhost");
        int port = Integer.parseInt(env("ACTIVEMQ_PORT", "1883"));
        final String destination = arg(args, 0, "/mqtt/krt");


        MQTT mqtt = new MQTT();
        mqtt.setHost(host, port);
        mqtt.setUserName(user);
        mqtt.setPassword(password);
        
        mqtt.setClientId("krt-client");
        mqtt.setCleanSession(false);
        
        FutureConnection connection = mqtt.futureConnection();
        Future<Void> f1 = connection.connect();
        f1.await();

        Topic[] topics = new Topic[]{new Topic("/mqtt/krt", QoS.AT_LEAST_ONCE)};
        Future<byte[]> f2 = connection.subscribe(topics);
        byte[] qoses = f2.await();
       
        int i = 0;
        while (true) {
            // We can start future receive..
            Future<Message> receive = connection.receive();

            // send the message..
            // Future<Void> f3 = connection.publish("foo", "Hello".getBytes(), QoS.AT_LEAST_ONCE, false);

            
            // Then the receive will get the message.
            List<Message> messages = new ArrayList<Message>();
            
            Message message = receive.await();
            messages.add(message);
            i++;

            if (i == 1) {
                int size = messages.size();
                for (int j = 0; j < size; j++) {
                    Message m = (Message) messages.get(j);
                    m.ack();
                }
                
                i = 0;
            }
            
            String msgStr = new String(message.getPayload());
            System.out.println(msgStr);
            if (msgStr.equals("shutdown")) {
                break;
            }
        }

        Future<Void> f4 = connection.disconnect();
        f4.await();
    }
    
    
    private static String env(String key, String defaultValue) {
        String rc = System.getenv(key);
        if( rc== null )
            return defaultValue;
        return rc;
    }
    
    
    private static String arg(String []args, int index, String defaultValue) {
        if( index < args.length )
            return args[index];
        else
            return defaultValue;
    }
}

