#! /usr/bin/env python
# coding: utf-8

import os
import sys
import time

import paho.mqtt.client as mqtt


if len(sys.argv) != 2:
    print "help"
    os._exit(1)

print sys.argv

try:
    is_durable = bool(int(sys.argv[1]))
except:
    print "help"
    os._exit(1)
    
print sys.argv
print "durable:", is_durable

if is_durable == True:
    clean_session = False
    queue_name = "/mqtt/durable"
    client_id = "mqtt-perf-durable"
else:
    queue_name = "/mqtt/nondurable"
    clean_session = True
    client_id = "mqtt-perf-nondurable"


ip = "192.168.239.136"
port = 1883
user = "test"
passwd = "test"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(queue_name, qos=1)


def on_message(client, userdata, msg):
    global count
    global fp
    count = count + 1
    body = str(msg.payload)
    fp.write(body)
    fp.write("\n")

    if body == "shutdown":
        fp.close()
        global start
        end = time.time()
        diff = end - start
        if diff == 0:
            diff = 0.001

        print "time: ", diff
        print "msg count: ", count + 1
        print "count per sec: ", count / diff
        client.loop_stop()


client = mqtt.Client(client_id=client_id, clean_session=clean_session)
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(user, password=passwd)
client.connect(ip, port, 60)

start = time.time()
count = 0
try:
    fp = open("test.txt", "wb")
except:
    print "open test.txt file error"
    os._exit(1)
    
client.loop_forever()
print "xxxxx"
client.disconnect()

