#! /usr/bin/env python

import os
import pika
import sys
import time

if len(sys.argv) != 4:
    print "help"
    os._exit(1)

print sys.argv

try:
    bytes_num = int(sys.argv[1])
    count = int(sys.argv[2])
    is_durable = bool(int(sys.argv[3]))
except:
    print "help"
    os._exit(1)
    
message = "x" * bytes_num

print "bytes:", bytes_num
print "count:", count
print "durable:", is_durable

if is_durable == True:
    durable = True
    delivery_mode = 2
    queue_name = "task_queue_durable"
else:
    queue_name = "non-task_queue_durable"
    durable = False
    delivery_mode = 1

    
credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.136", credentials=credential)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

#channel.exchange_declare(exchange="logs_durable", type="fanout", durable=True)
channel.queue_declare(queue=queue_name, durable=durable) 



start = time.time()
for i in xrange(0, count):
    channel.basic_publish(exchange="", routing_key=queue_name, body=message,
                          properties=pika.BasicProperties(delivery_mode=delivery_mode))

channel.basic_publish(exchange="", routing_key=queue_name, body="shutdown",
                      properties=pika.BasicProperties(delivery_mode=delivery_mode))

end = time.time()
diff = end - start
if diff == 0:
    diff = 0.001

print "time: ", diff
print "msg count: ", count + 1
print "count per sec: ", count / diff

# print " [x] sent %r" % (message)
connection.close()

