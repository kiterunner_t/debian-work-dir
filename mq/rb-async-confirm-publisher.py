#! /usr/bin/env python

import pika
import tornado.gen
import tornado.ioloop


def on_confirm(method):
    print method
    import os
    os._exit(0)


def on_publish(ch, method, prop, body):
    print method.reply_code
    import os
    


@tornado.gen.engine
def on_channel(channel):
    method = yield tornado.gen.Task(channel.exchange_declare, exchange='first', type='fanout')
    method = yield tornado.gen.Task(channel.queue_declare, queue='A')
    channel.confirm_delivery(callback=on_confirm, nowait=False)  # async confirm
    channel.add_on_return_callback(on_publish)
    channel.basic_publish(exchange='first', routing_key='', body='good', mandatory=True)
    #channel.close()


def on_connect(conn):
    conn.channel(on_open_callback=on_channel)


def publish():
    #credential = pika.PlainCredentials("test", "test")
    parameters = pika.ConnectionParameters(host="localhost")
    pika.TornadoConnection(parameters, on_open_callback=on_connect)


if __name__ == '__main__':
    publish()
    tornado.ioloop.IOLoop().current().start()

