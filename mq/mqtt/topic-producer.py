#! /usr/bin/env python
# coding: utf-8

import os
import sys
import time

import paho.mqtt.client as mqtt


queue_name = "/mqtt/nondurable"
clean_session = True
client_id = "mqtt-topic-producer"


ip = "127.0.0.1"
port = 1883
user = "test"
passwd = "test"


def on_connect(mqttc, obj, flags, rc):
    print("rc: "+str(rc))

def on_message(mqttc, obj, msg):
    print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

def on_publish(mqttc, obj, mid):
    print str(obj)
    print str(mid)
    

'''
def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)
'''

client = mqtt.Client(client_id=client_id, clean_session=clean_session)
#client.on_message = on_message
client.on_connect = on_connect
client.on_publish = on_publish
#client.on_subscribe = on_subscribe
#client.on_log = on_log

client.username_pw_set(user, password=passwd)
client.connect(ip, port, 60)

print "start publish ..."
infot = client.publish("config", "test-message", qos=1)


infot = client.publish("config/1", "test-message", qos=1)


infot = client.publish("config/2", "test-message", qos=1)

print "pub config"
#infot.wait_for_publish()

client.disconnect()

