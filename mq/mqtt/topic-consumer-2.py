#! /usr/bin/env python
# coding: utf-8

import os
import sys
import time

import paho.mqtt.client as mqtt


clean_session = True
client_id = "mqtt-topic-consumer-2"


ip = "127.0.0.1"
port = 1883
user = "test"
passwd = "test"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("config", qos=1)
    client.subscribe("config/2", qos=1)


def on_message(client, userdata, msg):
    body = str(msg.payload)

    print msg.topic, "--->>> ", body


client = mqtt.Client(client_id=client_id, clean_session=clean_session)
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(user, password=passwd)
client.connect(ip, port, 60)


client.loop_forever()
print "xxxxx"
client.disconnect()

