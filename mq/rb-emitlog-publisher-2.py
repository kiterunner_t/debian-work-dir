#!/usr/bin/env python

import pika
import sys


credential = pika.PlainCredentials("test", "test")
parameters = pika.ConnectionParameters(host="192.168.239.132",
                                       credentials=credential)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.exchange_declare(exchange="direct_logs", type="direct")

severity = sys.argv[1] if len(sys.argv) > 1 else "info"
message = " ".join(sys.argv[2:]) or "Hello World!"
channel.basic_publish(exchange="direct_logs",
                      routing_key=severity,
                      body=message)

print " [x] Sent %r:%r" % (severity, message)

connection.close()

